﻿using InfoFreightSystem;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SignalATM.Commons;
using SignalATM.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Timers;
using System.Web.Mvc;

namespace SignalATM.Web.Controllers
{
    public class AdminController : Controller
    {        
      
        SQLDataAccess sqlDataAccess;
        Hashtable inputParams;
        public GlobalVariables globalVariables;
        //public AdminController()
        //{
        //     string emailId = System.Web.HttpContext.Current.Session["UserId"].ToString();          
        //   // string emailId = "admin@gmail.com";            
        //    sqlDataAccess = new SQLDataAccess();
        //    //inputParams = new Hashtable();
        //    //inputParams.Add("@strApplicationUserLoginName", emailId);
        //    globalVariables = new GlobalVariables(emailId);

            
             
        //}
        public void SetGlobalMarket(int marketId)
        {
            globalVariables.MarketID = marketId;
        }
        //public void SetGlobalVariable()
        //{
        //    string emailId = System.Web.HttpContext.Current.Session["UserId"].ToString();
        //    sqlDataAccess = new SQLDataAccess();
        //    inputParams = new Hashtable();
        //    inputParams.Add("@strApplicationUserLoginName", emailId);
        //    globalVariables = new GlobalVariables();
        //    DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetGlobalVariablesData", inputParams);
        //    globalVariables.SecUserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
        //    globalVariables.RoleID = Convert.ToInt32(dt.Rows[0]["RoleID"]);
        //    globalVariables.SecApplicationUserID = Convert.ToInt32(dt.Rows[0]["ApplicationUserID"]);
        //    globalVariables.ApplicationUserEmailID = Convert.ToString(dt.Rows[0]["ApplicationUserEmailID"]);
        //    globalVariables.RoleName = dt.Rows[0]["RoleName"].ToString();
        //}
        public ActionResult Index()
        {
            return View();
        }
        #region AdminLogin
        public ActionResult Login(string emailId)
        {                   
          //  ViewBag.Message = "Your application description page.";
            return View();
        }       
        public JsonResult LoginDetailsAsync(string email, string password)
        {
            try
            {
                UserMgnt u = new UserMgnt();             
                Session["UserId"] = email;
                Hashtable hInput = new Hashtable();
                string pwd = u.Encrypt(password);
                hInput.Add("@iUserEmail", email);
                hInput.Add("@strApplicationUserLoginPassword", pwd);
                //SignalATMClass signalATM = new SignalATMClass();
                //string isUserExists = signalATM.CheckEmailAddress(hInput);
                string isUserExists = SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.CheckEmailExists, hInput);
                if (isUserExists != "0")
                {
                    u.Password = u.Encrypt(password);
                    //   string de = u.Decrypt("7ho0jeq6FwTMvOZOuJPmeQ==");
                    var request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["WebAPI_URL"].ToString() + "token");
                    var postData = "grant_type=" + Uri.EscapeDataString(ConfigurationManager.AppSettings["GrantType"].ToString());
                    postData += "&username=" + Uri.EscapeDataString(email);
                    //postData += "&password=" + Uri.EscapeDataString(txtPassword.Text);
                    postData += "&password=" + Uri.EscapeDataString(u.Password);
                    var data = Encoding.ASCII.GetBytes(postData);
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = data.Length;

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    if (responseString != "")
                    {
                        JObject jObject = JObject.Parse(responseString);
                        string Access_token = (string)jObject.SelectToken("access_token");
                        string token_type = (string)jObject.SelectToken("token_type");
                        string expires_in = (string)jObject.SelectToken("expires_in");
                        string UserName = (string)jObject.SelectToken("UserName");
                        System.Web.HttpCookie myCookie = new System.Web.HttpCookie("access_token");
                        Session["UserName"] = UserName;
                        Session["Access_token"] = Access_token;
                        Session["token_type"] = token_type;
                        Session["expires_in"] = expires_in;                     
                        return Json(new { success = true, result = "OK"  }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = true, result = "Failed" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = true, result = "NotExist" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { success = true, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region Common
        public ActionResult GetUserNameList()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            List<UserList> NewUserList = new List<UserList>();
            // inputParams.Add("@iUserID", globalVariables.SecUserID);
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Admin_GetUserNameList", inputParams);
            foreach (DataRow dr in result.Rows)
            {
                NewUserList.Add(new UserList
                {
                    UserID = Convert.ToInt32(dr["UserID"]),
                    UserName = dr["UserName"].ToString()
                });
            }

            return Json(NewUserList, JsonRequestBehavior.AllowGet);
            //ViewBag.UserList = new SelectList(result.ToString(), "UserID", "UserName");
            //string str = JsonConvert.SerializeObject(result);
            //return str;
        }
        public JsonResult GetEarningPeroidList()
        {

            List<SignalPerformanceEarningsPeriod> PeroidList = new List<SignalPerformanceEarningsPeriod>();
            sqlDataAccess = new SQLDataAccess();
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllEarningsPeriodList");

            foreach (DataRow dr in result.Rows)
            {
                PeroidList.Add(new SignalPerformanceEarningsPeriod
                {
                    SignalPerformanceEarningsPeriodID = Convert.ToInt16(dr["SignalPerformanceEarningsPeriodID"])
                    // Peroid= Convert.ToInt16(dr["SignalPerformanceEarningsPeriodID"])
                    // UserTypeName = dr["UserTypeName"].ToString()
                });
            }

            return Json(PeroidList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUserType()
        {

            List<secUserType> UserTypeList = new List<secUserType>();
            sqlDataAccess = new SQLDataAccess();
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserTypes");

            foreach (DataRow dr in result.Rows)
            {
                UserTypeList.Add(new secUserType
                {
                    UserTypeID = Convert.ToByte(dr["UserTypeID"]),
                    UserTypeName = dr["UserTypeName"].ToString()
                });
            }

            return Json(UserTypeList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUserPlanList()
        {

            List<UserPlan> UserPlanList = new List<UserPlan>();
            sqlDataAccess = new SQLDataAccess();
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserPlansList");

            foreach (DataRow dr in result.Rows)
            {
                UserPlanList.Add(new UserPlan
                {
                    UserPlansID = Convert.ToByte(dr["UserPlansID"]),
                    PlanName = dr["PlanName"].ToString()
                });
            }

            return Json(UserPlanList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllSignalType()
        {

            List<mstSignalType> SignalTypeList = new List<mstSignalType>();
            sqlDataAccess = new SQLDataAccess();
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSignalTypes");

            foreach (DataRow dr in result.Rows)
            {
                SignalTypeList.Add(new mstSignalType
                {
                    SignalTypesID = Convert.ToInt32(dr["SignalTypesID"]),
                    Stype = dr["Stype"].ToString()
                });
            }

            return Json(SignalTypeList, JsonRequestBehavior.AllowGet);
        }
       
        public ActionResult GetTimeZones()
        {
            List<Models.TimeZone> timezoneList = new List<Models.TimeZone>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTimeZones");
            foreach (DataRow r in dt.Rows)
            {
                timezoneList.Add(new Models.TimeZone
                {
                    TimeZoneID = Convert.ToInt32(r["TimeZoneID"]),
                    TimeZoneDetails = r["TimeZoneDetails"].ToString()
                });
            }
            return Json(new { success = true, TimeZoneslist = timezoneList }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetTimeZoneByCountryId(int Country)
        {
            List<Models.TimeZone> timezoneList = new List<Models.TimeZone>();

            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParams = new Hashtable();
            inputParams.Add("@iCountryID", Country);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTimeZonesByCountryId", inputParams);
            foreach (DataRow r in dt.Rows)
            {
                timezoneList.Add(new Models.TimeZone
                {
                    TimeZoneID = Convert.ToInt32(r["TimeZoneID"]),
                    TimeZoneDetails = r["TimeZoneDetails"].ToString()
                });
            }
            return Json(timezoneList, JsonRequestBehavior.AllowGet);
            // return Json(new { success = true, TimeZoneslist = timezoneList }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetStates()
        {
            List<mstState> stateList = new List<mstState>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetmstState");
            foreach (DataRow r in dt.Rows)
            {
                stateList.Add(new mstState
                {
                    StateID = Convert.ToInt32(r["StateID"]),
                    StateName = r["StateName"].ToString()
                });
            }
            return Json(stateList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetMessageChannels()
        {
            List<mstMessageChannel> MessageChannelsList = new List<mstMessageChannel>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetmstMessageChannels");
            foreach (DataRow r in dt.Rows)
            {
                MessageChannelsList.Add(new mstMessageChannel
                {
                    MessageChannelsID = Convert.ToInt32(r["MessageChannelsID"]),
                    ChannelType = r["ChannelType"].ToString()
                });
            }
            return Json(MessageChannelsList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetRolelist()
        {

            List<secRole> RoleList = new List<secRole>();
            sqlDataAccess = new SQLDataAccess();
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetRoles");

            foreach (DataRow dr in result.Rows)
            {
                RoleList.Add(new secRole
                {
                    RoleID = Convert.ToByte(dr["RoleID"]),
                    RoleName = dr["RoleName"].ToString()
                });
            }

            return Json(RoleList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPlanDetailsById(int UserPlansID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParams = new Hashtable();
            List<UserPlan> UserPlanList = new List<UserPlan>();
            inputParams.Add("@iUserPlansID", UserPlansID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserPlansById", inputParams);
            foreach (DataRow r in dt.Rows)
            {
                UserPlanList.Add(new Models.UserPlan
                {
                    Fee = Convert.ToDecimal(r["Fee"]),
                    Commission = Convert.ToDouble(r["Commission"]),
                    FixedAmount = Convert.ToDouble(r["FixedAmount"]),
                    Validity = Convert.ToInt16(r["Validity"])
                });
            }

            return Json(UserPlanList, JsonRequestBehavior.AllowGet);
            //return Json(new { success = true, result = UserPlanList }, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetMarketList()
        {
            try
            {
                JObject marketList = new JObject();
                List<mstMarket> marketResult = new List<mstMarket>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseUrl"].ToString());
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage resp = await client.GetAsync("serviceproviders/getmarkets");

                    if (resp.IsSuccessStatusCode)
                    {
                        var marketResponse = resp.Content.ReadAsStringAsync().Result;
                        marketList = (JObject)JsonConvert.DeserializeObject(marketResponse);
                        IList<JToken> results = marketList["data"].Children().ToList();
                        foreach (JToken dr in results)
                        {
                            marketResult.Add(new mstMarket
                            {
                                MarketID = Convert.ToInt32(dr["MarketID"]),
                                MarketName = dr["MarketName"].ToString()
                            });
                        }
                        return Json(marketResult, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> GetInstrumentList(int marketId)
        {
            JObject instList = new JObject();
            List<mstInstrument> instDataRes = new List<mstInstrument>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseUrl"].ToString());
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage resp = await client.GetAsync("serviceproviders/GetInstrumentsByMarket?MarketId=" + marketId);

                if (resp.IsSuccessStatusCode)
                {
                    var instResponse = resp.Content.ReadAsStringAsync().Result;
                    instList = (JObject)JsonConvert.DeserializeObject(instResponse);
                    IList<JToken> results = instList["data"].Children().ToList();
                    foreach (JToken dr in results)
                    {
                        instDataRes.Add(new mstInstrument
                        {
                            InstrumentID = Convert.ToInt32(dr["InstrumentID"]),
                            InstrumentsName = dr["InstrumentsName"].ToString()
                        });
                    }
                    return Json(instDataRes, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                }

            }
        }
        #endregion

        #region Application Setting

        #region ApplicationSetting/SignalRelated


        #region signal Type
        [HttpGet]      
        public ActionResult AddSignalType()
        {
            return View();
        }
        [HttpPost]
        public string AddSignalType(string Stype, decimal Price, decimal RefundPrice, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iSignalTypeID", 0);
            inputParam.Add("@strStype", Stype);
            inputParam.Add("@dPrice", Price);
            inputParam.Add("@dRefundPrice", RefundPrice);
            inputParam.Add("@iStatus", statusID);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_mstSignalTypesForConfig", inputParam);
            return res;
        }

        [HttpPost]
        public string EditSignalType(int SignalTypesID, string Stype, decimal Price, decimal RefundPrice, int Status)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iSignalTypeID", SignalTypesID);
            inputParam.Add("@strStype", Stype);
            inputParam.Add("@dPrice", Price);
            inputParam.Add("@dRefundPrice", RefundPrice);
            inputParam.Add("@iStatus", Status);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_mstSignalTypesForConfig", inputParam);
            return "Updated successfully";
        }
        [HttpPost]
        public string DeleteSignalType(int SignalTypesID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iSignalTypeID", SignalTypesID);
            inputParam.Add("@strStype", "");
            inputParam.Add("@dPrice", 1.0);
            inputParam.Add("@dRefundPrice", 1.0);
            inputParam.Add("@iStatus", "");
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_mstSignalTypesForConfig", inputParam);
            return "Delete successfully";
        }
        public string GetSignalTypes()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_mstSignalTypesForConfig");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }


        #endregion
        #region Risk 
        public ActionResult AddRiskType()
        {
            return View();
        }
        //Adding risk 
        [HttpPost]
        public ActionResult AddRiskType(string RiskName, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@RiskName", RiskName);
            inputParam.Add("@StatusID", statusID);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_SaveRisk", inputParam);
            return View();
        }
        //Delete risk by risk id
        [HttpPost]
        public string DeleteRisk(int riskID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@RiskID", riskID);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteRiskById", inputParam);
            return "Delete successfully";
        }
        //Edit risk by risk id
        [HttpPost]
        public string EditRisk(int riskID, string riskName, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@RiskName", riskName);
            inputParam.Add("@StatusID", statusID);
            inputParam.Add("@RiskID", riskID);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateRisk", inputParam);
            return "Updated successfully";
        }
        public JsonResult GetStatus()
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                DataTable res = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetStatus");
                string str = JsonConvert.SerializeObject(res);
                //  JObject statusList = new JObject();
                List<mstStatus> statusResult = new List<mstStatus>();
                foreach (DataRow dr in res.Rows)
                {
                    statusResult.Add(new mstStatus
                    {
                        StatusID = Convert.ToInt32(dr["StatusID"]),
                        Status = dr["Status"].ToString()
                    });
                }
                return Json(statusResult, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetRisks()
        {
            sqlDataAccess = new SQLDataAccess();
            //  List<mstRisk> riskList = new List<mstRisk>();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllRisksForConfig");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        #endregion

        #region Reliability
        public ActionResult AddReliability()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddReliability(string relabilityName, string relibilitySymbol, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@Name", relabilityName);
            inputParam.Add("@Symbol", relibilitySymbol);
            inputParam.Add("@StatusID", statusID);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_SaveRelibility", inputParam);
            return View();
        }
        public string EditReliability(int relibilityID, string reliablityName, string reliabilitySymbol, int statusId)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@ReliabilityID", relibilityID);
            inputParam.Add("@Name", reliablityName);
            inputParam.Add("@Symbol", reliabilitySymbol);
            inputParam.Add("@StatusID", statusId);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_update_mstReliabilityForConfig", inputParam);

            return "";
        }
        [HttpPost]
        public string DeleteRelibility(int relibilityID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@ReliabilityID", relibilityID);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteReliabilityById", inputParam);
            return "Delete successfully";
        }
        public string GetReliability()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllReliabilityForConfig");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        #endregion

        #region Validity
        public ActionResult AddValidity()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddValidity(int Period, string Unit, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iPeriod", Period);
            inputParam.Add("@sPeriodUnit", Unit);
            inputParam.Add("@iStatusID", statusID);
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iValidityID", "");

            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstValidityForConfig", inputParam);
            return View();
        }
        [HttpPost]
        public string EditValidity(int ValidityID, int Period, string Unit, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iPeriod", Period);
            inputParam.Add("@sPeriodUnit", Unit);
            inputParam.Add("@iStatusID", statusID);
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iValidityID", ValidityID);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstValidityForConfig", inputParam);
            return "Updated successfully";
        }

        public ActionResult EditDetailsValidity()
        {
            return View();
        }
        public string GetValidity()
        {
            sqlDataAccess = new SQLDataAccess();
            //  List<mstRisk> riskList = new List<mstRisk>();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iValidityID", -1);
            inputParam.Add("@iPeriod", "");
            inputParam.Add("@sPeriodUnit", "");
            inputParam.Add("@iStatusID", 0);


            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstValidityForConfig", inputParam);

            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        [HttpPost]
        public string DeleteValidity(int ValidityID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iValidityID", ValidityID);
            inputParam.Add("@iPeriod", "");
            inputParam.Add("@sPeriodUnit", "");
            inputParam.Add("@iStatusID", 0);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstValidityForConfig", inputParam);
            return "Delete successfully";
        }

        #endregion
        #region  TimeFrame
        public ActionResult AddTimeFrame()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddTimeframe(int Period, string Unit, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iPeriod", Period);
            inputParam.Add("@sPeriodUnit", Unit);
            inputParam.Add("@iStatusID", statusID);
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iTimeFrameID", "");
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstTimeFrameForConfig", inputParam);
            return View();

        }
        [HttpPost]
        public string EditTimeFrame(int TimeFrameID, int Period, string Unit, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iTimeFrameID", TimeFrameID);
            inputParam.Add("@iPeriod", Period);
            inputParam.Add("@sPeriodUnit", Unit);
            inputParam.Add("@iStatusID", statusID);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstTimeFrameForConfig", inputParam);
            return "Updated successfully";
        }
        public string GetTimeframe()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iTimeFrameID", 0);
            inputParam.Add("@iPeriod", "");
            inputParam.Add("@sPeriodUnit", "");
            inputParam.Add("@iStatusID", 0);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstTimeFrameForConfig", inputParam);

            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        [HttpPost]
        public string DeleteTimeFrame(int TimeFrameID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iTimeFrameID", TimeFrameID);
            inputParam.Add("@iPeriod", "");
            inputParam.Add("@sPeriodUnit", "");
            inputParam.Add("@iStatusID", 0);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstTimeFrameForConfig", inputParam);
            return "Delete successfully";
        }

        #endregion
        #region Success Status
        public ActionResult AddSuccess()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddSuccess(string Status)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iSuccessStatusID", "");
            inputParam.Add("@sSuccessStatus", Status);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstSuccessStatusForConfig", inputParam);
            return View();

        }
        [HttpPost]
        public string EditSuccess(int SuccessStatusID, string SuccessStatus)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iSuccessStatusID", SuccessStatusID);
            inputParam.Add("@sSuccessStatus", SuccessStatus);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstSuccessStatusForConfig", inputParam);
            return "Updated successfully";
        }
        public string GetSuccessStatus()
        {
            sqlDataAccess = new SQLDataAccess();
            //  List<mstRisk> riskList = new List<mstRisk>();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iSuccessStatusID", -1);
            inputParam.Add("@sSuccessStatus", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstSuccessStatusForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string DeleteSuccess(int SuccessStatusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iSuccessStatusID", SuccessStatusID);
            inputParam.Add("@sSuccessStatus", "");
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstSuccessStatusForConfig", inputParam);
            return "";
        }
        #endregion

        #region Process Status
        public ActionResult AddProcess()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddProcess(string Status)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iProcessStatusID", "");
            inputParam.Add("@sProcessStatus", Status);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstProcessStatusForConfig", inputParam);
            return View();

        }
        [HttpPost]
        public string EditProcess(int ProcessStatusID, string ProcessStatus)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iProcessStatusID", ProcessStatusID);
            inputParam.Add("@sProcessStatus", ProcessStatus);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstProcessStatusForConfig", inputParam);
            return "Updated successfully";
        }
        public string GetProcessStatus()
        {
            sqlDataAccess = new SQLDataAccess();
            //  List<mstRisk> riskList = new List<mstRisk>();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iProcessStatusID", "");
            inputParam.Add("@sProcessStatus", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstProcessStatusForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string DeleteProcess(int ProcessStatusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iProcessStatusID", ProcessStatusID);
            inputParam.Add("@sProcessStatus", "");
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstProcessStatusForConfig", inputParam);
            return "";
        }
        #endregion

        #region Buy/Sell
        public ActionResult AddBuySell()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddBuySell(string buysell)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iBuySellID", "");
            inputParam.Add("@sBuySell", buysell);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstBuySellForConfig", inputParam);
            return View();

        }
        [HttpPost]
        public string EditBuySell(int BuySellID, string BuySell)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iBuySellID", BuySellID);
            inputParam.Add("@sBuySell", BuySell);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstBuySellForConfig", inputParam);
            return "Updated successfully";
        }
        public string GetBuysell()
        {
            sqlDataAccess = new SQLDataAccess();
            //  List<mstRisk> riskList = new List<mstRisk>();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iBuySellID", "");
            inputParam.Add("@sBuySell", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstBuySellForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string DeleteBuysell(int ID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iBuySellID", ID);
            inputParam.Add("@sBuySell", "");
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstBuySellForConfig", inputParam);
            return "";
        }
        #endregion
        #endregion
        #region ApplicationSetting/UserRelated

        #region PIPs/Percent
        public ActionResult AddPercent()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddPercent(string percent, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iPipsOrPercentID", "");
            inputParam.Add("@sPipsOrPercentName", percent);
            inputParam.Add("@iStatusID", statusID);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstPipsPercentForConfig", inputParam);
            return View();

        }
        [HttpPost]
        public string EditPipPercent(int PipsOrPercentID, string percent, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iPipsOrPercentID", PipsOrPercentID);
            inputParam.Add("@sPipsOrPercentName", percent);
            inputParam.Add("@iStatusID", statusID);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstPipsPercentForConfig", inputParam);
            return "Updated successfully";
        }
        public string Deletepercent(int Id)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iPipsOrPercentID", Id);
            inputParam.Add("@sPipsOrPercentName", "");
            inputParam.Add("@iStatusID", "");
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_insert_update_delete_select_mstPipsPercentForConfig", inputParam);
            return res;
        }
        public string Getpercent()
        {
            sqlDataAccess = new SQLDataAccess();
            //  List<mstRisk> riskList = new List<mstRisk>();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iPipsOrPercentID", "");
            inputParam.Add("@sPipsOrPercentName", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstPipsPercentForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public JsonResult Getpercentlist()
        {

            List<mstPipsPercent> pipspercentList = new List<mstPipsPercent>();
            sqlDataAccess = new SQLDataAccess();
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPipsPercent");

            foreach (DataRow dr in result.Rows)
            {
                pipspercentList.Add(new mstPipsPercent
                {
                    PipsOrPercentID = Convert.ToInt32(dr["PipsOrPercentID"]),
                    PipsOrPercentName = dr["PipsOrPercentName"].ToString()
                });
            }

            return Json(pipspercentList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Role
        public ActionResult AddRole()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddRole(string RoleName, int UserTypeName, int Status)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iRoleID", "");
            inputParam.Add("@strRoleName", RoleName);
            inputParam.Add("@iRoleModifiedBy", 1);
            inputParam.Add("@dtRoleModifiedOn", Convert.ToDateTime(DateTime.UtcNow));
            inputParam.Add("@strRoleRefCode", 1);
            inputParam.Add("@nRoleRefID", 1);
            inputParam.Add("@iUserTypeID ", Convert.ToByte(UserTypeName));
            inputParam.Add("@iStatusID", Status);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_secRolesForConfig", inputParam);
            return View();
        }

        [HttpPost]
        public string EditRole(int RoleID, string RoleName, int UserTypeName, int Status)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iRoleID", Convert.ToInt32(RoleID));
            inputParam.Add("@strRoleName", Convert.ToString(RoleName));
            inputParam.Add("@iRoleModifiedBy", 1);
            inputParam.Add("@dtRoleModifiedOn", Convert.ToDateTime(DateTime.UtcNow));
            inputParam.Add("@strRoleRefCode", 1);
            inputParam.Add("@nRoleRefID", 1);
            inputParam.Add("@iUserTypeID ", UserTypeName);
            inputParam.Add("@iStatusID", Status);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_secRolesForConfig", inputParam);
            return "Updated successfully";
        }

        public string GetallRoleDetails()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iRoleID", 1);
            inputParam.Add("@strRoleName", "");
            inputParam.Add("@iRoleModifiedBy", 1);
            inputParam.Add("@dtRoleModifiedOn", Convert.ToDateTime(DateTime.UtcNow));
            inputParam.Add("@strRoleRefCode", 1);
            inputParam.Add("@nRoleRefID", 1);
            inputParam.Add("@iUserTypeID ", Convert.ToByte(1));
            inputParam.Add("@iStatusID", 1);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_secRolesForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        [HttpPost]
        public string DeleteRole(int RoleID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iRoleID", RoleID);
            inputParam.Add("@strRoleName", "");
            inputParam.Add("@iRoleModifiedBy", 1);
            inputParam.Add("@dtRoleModifiedOn", Convert.ToDateTime(DateTime.UtcNow));
            inputParam.Add("@strRoleRefCode", 1);
            inputParam.Add("@nRoleRefID", 1);
            inputParam.Add("@iUserTypeID ", Convert.ToByte(1));
            inputParam.Add("@iStatusID", 1);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_secRolesForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return "Delete successfully";
        }


        #endregion
        #region Gender

        public ActionResult AddGender()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddGender(string Gender)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iGenderID", "");
            inputParam.Add("@sGender", Gender);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstGenderForConfig", inputParam);
            return View();

        }
        [HttpPost]
        public string EditGender(int GenderID, string Gender)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iGenderID", GenderID);
            inputParam.Add("@sGender", Gender);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstGenderForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return "Updated successfully";
        }
        public string GetGender()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iGenderID", "");
            inputParam.Add("@sGender", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstGenderForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string DeletetGender(int GenderID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iGenderID", GenderID);
            inputParam.Add("@sGender", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstGenderForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion

        #region Country 
        public ActionResult AddCountry()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddCountry(string Name, int code, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iCountryID", "");
            inputParam.Add("@iCountryCode", code);
            inputParam.Add("@sCountry", Name);
            inputParam.Add("@iStatusID", statusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstCountryForConfig", inputParam);
            return View();

        }

        public string GetCountry()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iCountryID", "");
            inputParam.Add("@iCountryCode", "");
            inputParam.Add("@sCountry", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstCountryForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string Inactivecountry(int countryid, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iCountryID", countryid);
            inputParam.Add("@iCountryCode", "");
            inputParam.Add("@sCountry", "");
            inputParam.Add("@iStatusID", statusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstCountryForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        public JsonResult GetCountries()
        {
            List<mstCountry> countryList = new List<mstCountry>();
            sqlDataAccess = new SQLDataAccess();
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCountry");

            foreach (DataRow dr in result.Rows)
            {
                countryList.Add(new mstCountry
                {
                    CountryID = Convert.ToInt32(dr["CountryID"]),
                    Country = dr["Country"].ToString()
                });
            }

            return Json(countryList, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Timezone
        public ActionResult AddTimeZone()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddTimeZone(string TimeZoneDetails, int Country)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iTimeZoneID", "");
            inputParam.Add("@strTimeZoneDetails", Convert.ToString(TimeZoneDetails));
            inputParam.Add("@fTimeZoneDiffrence", 0.0);
            inputParam.Add("@iCountryID", Convert.ToInt32(Country));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_Select_TimeZoneForConfig", inputParam);
            return View();

        }

        [HttpPost]
        public string EditTimeZone(int TimeZoneID, string TimeZoneDetails, int Country)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iTimeZoneID", TimeZoneID);
            inputParam.Add("@strTimeZoneDetails", Convert.ToString(TimeZoneDetails));
            inputParam.Add("@fTimeZoneDiffrence", 0.0);
            inputParam.Add("@iCountryID", Convert.ToInt32(Country));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_Select_TimeZoneForConfig", inputParam);
            return "Updated successfully";
        }
        public string GetTimeZone()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iTimeZoneID", "");
            inputParam.Add("@strTimeZoneDetails", "");
            inputParam.Add("@fTimeZoneDiffrence", 0.0);
            inputParam.Add("@iCountryID", 1);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_Select_TimeZoneForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion

        #region State
        public ActionResult AddState()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddState(string StateName, int Country, int TimeZoneID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iStateID", "");
            inputParam.Add("@strStateName", StateName);
            inputParam.Add("@iTimeZoneID", TimeZoneID);
            inputParam.Add("@iCountryID", Country);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_Select_mstStateForConfig", inputParam);
            return View();
        }

        [HttpPost]
        public string EditState(int StateID, string StateName, int Country, int TimeZoneDetails)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iStateID", Convert.ToInt32(StateID));
            inputParam.Add("@strStateName", Convert.ToString(StateName));
            inputParam.Add("@iTimeZoneID", Convert.ToInt32(TimeZoneDetails));
            inputParam.Add("@iCountryID", Convert.ToInt32(Country));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_Select_mstStateForConfig", inputParam);
            return "Updated successfully";
        }
        public string GetAllStates()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iStateID", "");
            inputParam.Add("@strStateName", "");
            inputParam.Add("@iTimeZoneID", 1);
            inputParam.Add("@iCountryID", 1);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_Select_mstStateForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }


        #endregion

        #region City

        public ActionResult AddCity()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddCity(string CityName, int StateName)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iCityID", "");
            inputParam.Add("@strCityName", CityName);
            inputParam.Add("@iStateID", StateName);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_Select_mstCityForConfig", inputParam);
            return View();
        }
        [HttpPost]
        public string EditCity(int CityID, string CityName, int StateName)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iCityID", CityID);
            inputParam.Add("@strCityName", CityName);
            inputParam.Add("@iStateID", StateName);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_Select_mstCityForConfig", inputParam);
            return "Updated successfully";
        }

        public string GetAllCitys()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iCityID", "");
            inputParam.Add("@strCityName", "");
            inputParam.Add("@iStateID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_Select_mstCityForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion

        #region Ticket Status
        public ActionResult AddTicketStatus()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddTicketstatus(string Status, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iTicketStatusID", "");
            inputParam.Add("@sTicketStatus", Status);
            inputParam.Add("@iStatusID", statusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstTicketStatusForConfig", inputParam);
            return View();

        }
        [HttpPost]
        public string EditTicket(int TicketID, string Status, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iTicketStatusID", TicketID);
            inputParam.Add("@sTicketStatus", Status);
            inputParam.Add("@iStatusID", statusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstTicketStatusForConfig", inputParam);
            return "Updated successfully";
        }
        public string GetTicketStatus()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iTicketStatusID", "");
            inputParam.Add("@sTicketStatus", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstTicketStatusForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string DeletetTicketstatus(int TicketId)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iTicketStatusID", TicketId);
            inputParam.Add("@sTicketStatus", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstTicketStatusForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion


        #region  AddReferralStatus Status
        public ActionResult AddReferralStatus()
        {
            return View();
        }
        [HttpPost]
        public string AddRefererstatus(string RStatus, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iReferrerStatusID", "");
            inputParam.Add("@sReferrerStatus", RStatus);
            inputParam.Add("@iStatusID", statusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstReferrerStatusForConfig", inputParam);
            return "";

        }
        [HttpPost]
        public string EditReferral(int ReferrerStatusID, string ReferrerStatus, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iReferrerStatusID", ReferrerStatusID);
            inputParam.Add("@sReferrerStatus", ReferrerStatus);
            inputParam.Add("@iStatusID", statusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstReferrerStatusForConfig", inputParam);
            return "Updated successfully";
        }
        public string GetReferrerStatus()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iReferrerStatusID", "");
            inputParam.Add("@sReferrerStatus", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstReferrerStatusForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        public string DeleteReferrerstatus(int ReferrerId)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iReferrerStatusID", ReferrerId);
            inputParam.Add("@sReferrerStatus", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstReferrerStatusForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion
        #region Message Channels
        public ActionResult AddMessageChannel()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddMessagechannel(string Msgchannel, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iMessageChannelsID", "");
            inputParam.Add("@sChannelType", Msgchannel);
            inputParam.Add("@iStatusID", statusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstMessageChannelsForConfig", inputParam);
            return View();

        }
        [HttpPost]
        public string EditMsgChannel(int MessageChannelsID, string ChannelType, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iMessageChannelsID", MessageChannelsID);
            inputParam.Add("@sChannelType", ChannelType);
            inputParam.Add("@iStatusID", statusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstMessageChannelsForConfig", inputParam);
            return "Updated successfully";
        }
        public string Getmsgchannel()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iMessageChannelsID", "");
            inputParam.Add("@sChannelType", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstMessageChannelsForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string Deletemsgchnnel(int MessageChannelsID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iMessageChannelsID", MessageChannelsID);
            inputParam.Add("@sChannelType", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstMessageChannelsForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion
        #endregion


        #region ApplicationSetting/PaymentRelated
        #region Payment Type
        public ActionResult AddPaymentType()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddPaymenttype(string paymenttype, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iPaymentTypeID", "");
            inputParam.Add("@sPaymentType", paymenttype);
            inputParam.Add("@iStatusID", statusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstPaymentTypeForConfig", inputParam);
            return View();

        }
        [HttpPost]
        public string EditPaymentType(int PaymentTypeID, string PaymentType, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iPaymentTypeID", Convert.ToInt32(PaymentTypeID));
            inputParam.Add("@sPaymentType", Convert.ToString(PaymentType));
            inputParam.Add("@iStatusID", Convert.ToInt32(statusID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstPaymentTypeForConfig", inputParam);
            return "Updated successfully";
        }
        public string Deletepaymenttype(int PaymentTypeID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iPaymentTypeID", PaymentTypeID);
            inputParam.Add("@sPaymentType", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstPaymentTypeForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string Getpaymenttype()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iPaymentTypeID", "");
            inputParam.Add("@sPaymentType", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstPaymentTypeForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        #endregion

        #region Payment Source
        public ActionResult AddPaymentSource()
        {
            return View();
        }
        [HttpPost]
        public string AddpaymentSouce(string Name, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@ipaymentSourcesID", "");
            inputParam.Add("@sName", Name);
            inputParam.Add("@iStatusID", statusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstPaymentSourcesForConfig", inputParam);
            return "";
        }
        [HttpPost]
        public string EditPaymentSource(int paymentSourcesID, string Name, int status)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@ipaymentSourcesID", paymentSourcesID);
            inputParam.Add("@sName", Name);
            inputParam.Add("@iStatusID", status);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstPaymentSourcesForConfig", inputParam);
            return "Updated successfully";
        }

        public string Getpaymentsource()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@ipaymentSourcesID", "");
            inputParam.Add("@sName", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstPaymentSourcesForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string Deletepaymentsource(int paymentSourcesID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@ipaymentSourcesID", paymentSourcesID);
            inputParam.Add("@sName", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstPaymentSourcesForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion
        #region Paymentstatus
        public ActionResult AddPaymentStatus()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddPaymentstatus(string paymentstatus, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@ipaymentStatusID", "");
            inputParam.Add("@sPaymentStatus", paymentstatus);
            inputParam.Add("@iStatusID", statusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstPaymentStatusForConfig", inputParam);
            return View();

        }
        [HttpPost]
        public string EditPaymentStatus(int paymentStatusID, string PaymentStatus, int status)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@ipaymentStatusID", paymentStatusID);
            inputParam.Add("@sPaymentStatus", PaymentStatus);
            inputParam.Add("@iStatusID", status);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstPaymentStatusForConfig", inputParam);
            return "Updated successfully";
        }
        public string Deletepaymentstatus(int paymentStatusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@ipaymentStatusID", paymentStatusID);
            inputParam.Add("@sPaymentStatus", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstPaymentStatusForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string Getpaymentstatus()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@ipaymentStatusID", "");
            inputParam.Add("@sPaymentStatus", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstPaymentStatusForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion
        #endregion
        #endregion

        #region Opertional  Setting

        #region SignalRelated
        #region Market

        public ActionResult AddMarket()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddMarket(string MarketName, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iMarketID", "");
            inputParam.Add("@sMarketName", MarketName);
            inputParam.Add("@iStatusID", statusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstMarketsForConfig", inputParam);
            return View();

        }
        [HttpPost]
        public string EditMarketName(int MarketID, string MarketName, int status)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iMarketID", MarketID);
            inputParam.Add("@sMarketName", MarketName);
            inputParam.Add("@iStatusID", status);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstMarketsForConfig", inputParam);
            return "Updated successfully";
        }
        public string Deletemarkets(int MarketID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iMarketID", MarketID);
            inputParam.Add("@sMarketName", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstMarketsForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetAllmarkets()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iMarketID", "");
            inputParam.Add("@sMarketName", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstMarketsForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        #endregion

        #region Instrument

        public ActionResult AddInstrument()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddInstrument(int MarketID, int PipPercentId, decimal Multiflying, string ScriptName, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", Convert.ToInt32(1));
            inputParam.Add("@iInstrumentID", Convert.ToInt32(0));
            inputParam.Add("@iMarketID", Convert.ToInt32(MarketID));
            inputParam.Add("@strInstrumentsName", Convert.ToString(ScriptName));
            inputParam.Add("@dMultiplier", Convert.ToDecimal(Multiflying));
            inputParam.Add("@ipipsOrPercentId", Convert.ToInt32(PipPercentId));
            inputParam.Add("@siDecimalCount", Convert.ToInt32(2));
            inputParam.Add("@iStatusID", Convert.ToInt32(statusID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstInstrumentsForConfig", inputParam);
            return View();

        }

        [HttpPost]
        public string EditInstrument(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", Convert.ToInt32(2));
            inputParam.Add("@iInstrumentID", Convert.ToInt32(obj.InstrumentID));
            inputParam.Add("@iMarketID", Convert.ToInt32(obj.marketid));
            inputParam.Add("@strInstrumentsName", Convert.ToString(obj.scriptname));
            inputParam.Add("@dMultiplier", Convert.ToDecimal(obj.multif));
            inputParam.Add("@ipipsOrPercentId", Convert.ToInt32(obj.pippercentid));
            inputParam.Add("@siDecimalCount", Convert.ToInt32(2));
            inputParam.Add("@iStatusID", Convert.ToInt32(obj.statusId));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstInstrumentsForConfig", inputParam);
            return "";

        }

        [HttpPost]
        public string DeleteInstrument(int InstrumentID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", Convert.ToInt32(4));
            inputParam.Add("@iInstrumentID", Convert.ToInt32(InstrumentID));
            inputParam.Add("@iMarketID", Convert.ToInt32(1));
            inputParam.Add("@strInstrumentsName", Convert.ToString(""));
            inputParam.Add("@dMultiplier", Convert.ToDecimal(0));
            inputParam.Add("@ipipsOrPercentId", Convert.ToInt32(1));
            inputParam.Add("@siDecimalCount", Convert.ToInt32(2));
            inputParam.Add("@iStatusID", Convert.ToInt32(1));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstInstrumentsForConfig", inputParam);
            return "";
        }
        public string GetallInstruments()
        {
            sqlDataAccess = new SQLDataAccess();
            //  List<mstRisk> riskList = new List<mstRisk>();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iInstrumentID", "");
            inputParam.Add("@iMarketID", "");
            inputParam.Add("@strInstrumentsName", "");
            inputParam.Add("@dMultiplier", 0.0);
            inputParam.Add("@ipipsOrPercentId", "");
            inputParam.Add("@siDecimalCount", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_mstInstrumentsForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion

        #region Earning Period
        public ActionResult AddEarningPeriod()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddEarningPeriod(int MarketID, string StartFrom, string ValidTill, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iSignalPerformanceEarningsPeriodID", "");
            inputParam.Add("@iMarketID", MarketID);
            inputParam.Add("@dtStartFrom", StartFrom);
            inputParam.Add("@dtValidTill", ValidTill);
            inputParam.Add("@iStatusID", statusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_SignalPerformanceEarningsPeriodForConfig", inputParam);
            return View();

        }
        [HttpPost]
        public string EditEarningPeriod(int SignalPerformanceEarningsPeriodID, int MarketID, string StartFrom, string ValidTill, int statusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iSignalPerformanceEarningsPeriodID", SignalPerformanceEarningsPeriodID);
            inputParam.Add("@iMarketID", MarketID);
            inputParam.Add("@dtStartFrom", StartFrom);
            inputParam.Add("@dtValidTill", ValidTill);
            inputParam.Add("@iStatusID", statusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_SignalPerformanceEarningsPeriodForConfig", inputParam);
            return "Updated successfully";
        }

        public string GetEarningsPeriod()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iSignalPerformanceEarningsPeriodID", "");
            inputParam.Add("@iMarketID", "");
            inputParam.Add("@dtStartFrom", "");
            inputParam.Add("@dtValidTill", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_SignalPerformanceEarningsPeriodForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string DeleteEarningperiod(int SignalPerformanceEarningsPeriodID)
        {

            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iSignalPerformanceEarningsPeriodID", SignalPerformanceEarningsPeriodID);
            inputParam.Add("@iMarketID", "");
            inputParam.Add("@dtStartFrom", "");
            inputParam.Add("@dtValidTill", "");
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_SignalPerformanceEarningsPeriodForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }


        public JsonResult Getmarkets()
        {
            List<mstMarket> MarketList = new List<mstMarket>();
            sqlDataAccess = new SQLDataAccess();
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetMarkets");

            foreach (DataRow dr in result.Rows)
            {
                MarketList.Add(new mstMarket
                {
                    MarketID = Convert.ToInt32(dr["MarketID"]),
                    MarketName = dr["MarketName"].ToString()
                });
            }

            return Json(MarketList, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region EmpanelmentCriteria Criteria
        public ActionResult AddEmpanelmentCriteria()
        {
            return View();
        }

        [HttpPost]
        public string AddEmpanelmentCriteria(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iSignalProviderEmpanelledCriteriaID", 0);
            inputParam.Add("@iPipsOrPercentID", Convert.ToInt32(obj.PipsOrPercentID));
            inputParam.Add("@sdtFromDate", Convert.ToDateTime(obj.FromDate));
            inputParam.Add("@sdtToDate", Convert.ToDateTime(obj.ToDate));
            inputParam.Add("@iCriteriaDays", Convert.ToInt32(obj.CriteriaDays));
            inputParam.Add("@dMinSuccessRate ", Convert.ToDecimal(obj.MinSuccessRate));
            inputParam.Add("@dMINTotalGainedPips", Convert.ToDecimal(obj.MINTotalGainedPips));
            inputParam.Add("@dMinAverageGainedPips", Convert.ToDecimal(obj.MinAverageGainedPips));
            inputParam.Add("@dGainedLostPipsRatio", Convert.ToDecimal(obj.GainedLostPipsRatio));
            inputParam.Add("@iMINTotalSignalCount", Convert.ToInt32(obj.MINTotalSignalCount));
            inputParam.Add("@iStatusID", Convert.ToInt32(obj.StatusID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_SignalProviderEmpanelledCriteriaForConfig", inputParam);

            string str = JsonConvert.SerializeObject(dt);
            return str;
        }


        [HttpPost]
        public string EditEmpanelmentCriteria(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iSignalProviderEmpanelledCriteriaID", Convert.ToInt32(obj.SignalProviderEmpanelledCriteriaID));
            inputParam.Add("@iPipsOrPercentID", Convert.ToInt32(obj.PipsOrPercentID));
            inputParam.Add("@sdtFromDate", Convert.ToDateTime(obj.FromDate));
            inputParam.Add("@sdtToDate", Convert.ToDateTime(obj.ToDate));
            inputParam.Add("@iCriteriaDays", Convert.ToInt32(obj.CriteriaDays));
            inputParam.Add("@dMinSuccessRate ", Convert.ToDecimal(obj.MinSuccessRate));
            inputParam.Add("@dMINTotalGainedPips", Convert.ToDecimal(obj.MINTotalGainedPips));
            inputParam.Add("@dMinAverageGainedPips", Convert.ToDecimal(obj.MinAverageGainedPips));
            inputParam.Add("@dGainedLostPipsRatio", Convert.ToDecimal(obj.GainedLostPipsRatio));
            inputParam.Add("@iMINTotalSignalCount", Convert.ToInt32(obj.MINTotalSignalCount));
            inputParam.Add("@iStatusID", Convert.ToInt32(obj.StatusID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_SignalProviderEmpanelledCriteriaForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        [HttpPost]
        public string DeleteEmpanelmentCriteria(int SignalProviderEmpanelledCriteriaID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            // dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iSignalProviderEmpanelledCriteriaID", Convert.ToInt32(SignalProviderEmpanelledCriteriaID));
            inputParam.Add("@iPipsOrPercentID", Convert.ToInt32(0));
            inputParam.Add("@sdtFromDate", Convert.ToDateTime(DateTime.UtcNow));
            inputParam.Add("@sdtToDate", Convert.ToDateTime(DateTime.UtcNow));
            inputParam.Add("@iCriteriaDays", Convert.ToInt32(0));
            inputParam.Add("@dMinSuccessRate ", Convert.ToDecimal(0.0));
            inputParam.Add("@dMINTotalGainedPips", Convert.ToDecimal(0.0));
            inputParam.Add("@dMinAverageGainedPips", Convert.ToDecimal(0.0));
            inputParam.Add("@dGainedLostPipsRatio", Convert.ToDecimal(0.0));
            inputParam.Add("@iMINTotalSignalCount", Convert.ToInt32(1));
            inputParam.Add("@iStatusID", Convert.ToInt32(1));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_SignalProviderEmpanelledCriteriaForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        public string GetEmpanelmentCriteria()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllEmpanelledCriteria");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion
        #region Instrument Criteria
        public ActionResult AddInstrumentCriteria()
        {
            return View();
        }
        [HttpPost]
        public string AddInstrumentCriteria(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iSignalProviderInstrumentDisplayCriteriaID", 0);
            inputParam.Add("@iMarketID", Convert.ToInt32(obj.marketId));
            inputParam.Add("@dMinSuccessRate", Convert.ToDecimal(obj.successRate));
            inputParam.Add("@dMINTotalGainedPips", Convert.ToDecimal(obj.gainnedPips));
            inputParam.Add("@dMinAverageGainedPips", Convert.ToDecimal(obj.gainnedRates));
            inputParam.Add("@dGainedLostPipsRatio", Convert.ToDecimal(obj.pipsRatio));
            inputParam.Add("@iMINTotalSignalCount", Convert.ToInt32(obj.signalCount));
            inputParam.Add("@dPremiumSuccessRate", Convert.ToDecimal(obj.premiumrates));
            inputParam.Add("@iStatusID", Convert.ToInt32(obj.stausVal));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_SignalProviderInstrumentDisplayCriteriaForConfig", inputParam);

            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        [HttpPost]
        public string EditInstrumentCriteria(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iSignalProviderInstrumentDisplayCriteriaID", Convert.ToInt32(obj.SPId));
            inputParam.Add("@iMarketID", Convert.ToInt32(obj.marketId));
            inputParam.Add("@dMinSuccessRate", Convert.ToDecimal(obj.successRate));
            inputParam.Add("@dMINTotalGainedPips", Convert.ToDecimal(obj.gainnedPips));
            inputParam.Add("@dMinAverageGainedPips", Convert.ToDecimal(obj.gainnedRates));
            inputParam.Add("@dGainedLostPipsRatio", Convert.ToDecimal(obj.pipsRatio));
            inputParam.Add("@iMINTotalSignalCount", Convert.ToInt32(obj.signalCount));
            inputParam.Add("@dPremiumSuccessRate", Convert.ToDecimal(obj.premiumrates));
            inputParam.Add("@iStatusID", Convert.ToInt32(obj.stausVal));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_SignalProviderInstrumentDisplayCriteriaForConfig", inputParam);

            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        [HttpPost]
        public string DeleteInstrumentCriteria(int SignalProviderInstrumentDisplayCriteriaID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            // dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iSignalProviderInstrumentDisplayCriteriaID", Convert.ToInt32(SignalProviderInstrumentDisplayCriteriaID));
            inputParam.Add("@iMarketID", Convert.ToInt32(0));
            inputParam.Add("@dMinSuccessRate", Convert.ToDecimal(0.0));
            inputParam.Add("@dMINTotalGainedPips", Convert.ToDecimal(0.0));
            inputParam.Add("@dMinAverageGainedPips", Convert.ToDecimal(0.0));
            inputParam.Add("@dGainedLostPipsRatio", Convert.ToDecimal(0.0));
            inputParam.Add("@iMINTotalSignalCount", Convert.ToInt32(0));
            inputParam.Add("@dPremiumSuccessRate", Convert.ToDecimal(0.0));
            inputParam.Add("@iStatusID", Convert.ToInt32(0));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_SignalProviderInstrumentDisplayCriteriaForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetInstrumentCriteria()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 3);
            inputParam.Add("@iSignalProviderInstrumentDisplayCriteriaID", "");
            inputParam.Add("@iMarketID", "");
            inputParam.Add("@dMinSuccessRate", 0.0);
            inputParam.Add("@dMINTotalGainedPips", 0.0);
            inputParam.Add("@dMinAverageGainedPips", 0.0);
            inputParam.Add("@dGainedLostPipsRatio", 0.0);
            inputParam.Add("@iMINTotalSignalCount", "");
            inputParam.Add("@dPremiumSuccessRate", 0.0);
            inputParam.Add("@iStatusID", "");
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_select_SignalProviderInstrumentDisplayCriteriaForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        #endregion

        #region Earnings Critieria
        public ActionResult AddEarningsCritieria()
        {
            return View();
        }
        [HttpPost]
        public string AddEarningsCritieria(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iSignalProviderEarningRatesID", 0);
            inputParam.Add("@iPeriodID", Convert.ToInt32(obj.PeriodID));
            inputParam.Add("@iMarketId", Convert.ToDecimal(obj.MarketId));
            inputParam.Add("@dSuccessEarningsRate", Convert.ToDecimal(obj.SuccessEarningsRate));
            inputParam.Add("@dAveragePipsPercentRate", Convert.ToDecimal(obj.AveragePipsPercentRate));
            inputParam.Add("@dTradersSignalsPinnedRate", Convert.ToDecimal(obj.TradersSignalsPinnedRate));
            inputParam.Add("@iStatusID", Convert.ToInt32(obj.StatusID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_SignalProviderEarningRatesForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        [HttpPost]
        public string EditEarningsCritieria(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iSignalProviderEarningRatesID", Convert.ToInt32(obj.SignalProviderEarningRatesID));
            inputParam.Add("@iPeriodID", Convert.ToInt32(0));
            inputParam.Add("@iMarketId", Convert.ToInt32(0));
            inputParam.Add("@dSuccessEarningsRate", Convert.ToDecimal(obj.SuccessEarningsRate));
            inputParam.Add("@dAveragePipsPercentRate", Convert.ToDecimal(obj.AveragePipsPercentRate));
            inputParam.Add("@dTradersSignalsPinnedRate", Convert.ToDecimal(obj.TradersSignalsPinnedRate));
            inputParam.Add("@iStatusID", Convert.ToInt32(obj.StatusID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_SignalProviderEarningRatesForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        [HttpPost]
        public string DeleteEarningsCritieria(int SignalProviderEarningRatesID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 4);
            inputParam.Add("@iSignalProviderEarningRatesID", Convert.ToInt32(SignalProviderEarningRatesID));
            inputParam.Add("@iPeriodID", Convert.ToInt32(0));
            inputParam.Add("@iMarketId", Convert.ToInt32(0));
            inputParam.Add("@dSuccessEarningsRate", Convert.ToDecimal(0.0));
            inputParam.Add("@dAveragePipsPercentRate", Convert.ToDecimal(0.0));
            inputParam.Add("@dTradersSignalsPinnedRate", Convert.ToDecimal(0.0));
            inputParam.Add("@iStatusID", Convert.ToInt32(1));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_SignalProviderEarningRatesForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetEarningRates()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllSignalProviderEarningRates ");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetReffererEarnings()
        {
            sqlDataAccess = new SQLDataAccess();
            string str = "0";
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_admin_ReferrerEarnings");
            if(dt.Rows.Count>0)
            {
                str = JsonConvert.SerializeObject(dt);
            }         
            return str;
        }
        public string GetAllSigProviderEarnigs()
        {
            sqlDataAccess = new SQLDataAccess();
            string str = "0";
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllSignalProviderEarnings");
            if (dt.Rows.Count > 0)
            {
                str = JsonConvert.SerializeObject(dt);
            }
            return str;
        }
        #endregion
        #region Bonus Rate Slabs
        public ActionResult AddBonusrate()
        {
            return View();
        }
        [HttpPost]
        public string AddBonusrate(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            string str = "";
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iMarketID", Convert.ToInt32(obj.MarketID));
            inputParam.Add("@strSlabName", Convert.ToString(obj.SlabName));
            inputParam.Add("@dFromPipsPercentRate", Convert.ToDecimal(obj.From));
            inputParam.Add("@dToPipsPercentRate", Convert.ToDecimal(obj.To));
            inputParam.Add("@dBonusRate", Convert.ToDecimal(obj.Percent));
            inputParam.Add("@iStatusID", 1);
            var dt = sqlDataAccess.ExecuteStoreProcedure("usp_SaveBonusSlabRatesByAdmin", inputParam);
            if (dt != "")
            {
                str = dt;
            }
            return str;
        }
        public string GetSignalProviderBonusRates()
        {
            sqlDataAccess = new SQLDataAccess();
            string str = "";
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllSignalProviderBonusRates ");
            if (dt.Rows.Count > 0)
            {
                str = JsonConvert.SerializeObject(dt);
            }
            return str;
        }
        public string DeleteBonusrate(int BonusRatesID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            string str = "";
            inputParam.Add("@iSignalProviderBonusRatesID", BonusRatesID);
            inputParam.Add("@OperationalFlagID", 2);
            var dt = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteOrGetByIDBonusSlabRate", inputParam);
            if (dt != "")
            {
                str = dt;
            }
            return str;
        }
        public string GetSlabRateDetails(int BonusRatesID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            string str = "";
            inputParam.Add("@iSignalProviderBonusRatesID", BonusRatesID);
            inputParam.Add("@OperationalFlagID", 1);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_DeleteOrGetByIDBonusSlabRate", inputParam);
            if (dt.Rows.Count > 0)
            {
                str = JsonConvert.SerializeObject(dt);
            }
            return str;
        }
        #endregion


        #endregion

        #region User Related
        #region AddUserPlans
        public ActionResult AddUserPlans()
        {

            return View();
        }
        [HttpPost]
        public string AddUserPlans(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", Convert.ToInt32(1));
            inputParam.Add("@iUserPlansID", Convert.ToInt32(0));
            inputParam.Add("@strPlanName", Convert.ToString(obj.PName));
            inputParam.Add("@strRoleID", Convert.ToString(obj.RoleID));
            inputParam.Add("@dFee", Convert.ToDecimal(obj.fee));
            inputParam.Add("@fCommission", Convert.ToDecimal(obj.PCommission));
            inputParam.Add("@fFixedAmount", Convert.ToDecimal(obj.FAmount));
            inputParam.Add("@siValidity", Convert.ToInt32(obj.VDays));
            inputParam.Add("@iStatusID", Convert.ToInt32(obj.StatusId));
            inputParam.Add("@strPlanType", Convert.ToString(obj.PType));
            inputParam.Add("@strPlanPackage", Convert.ToString(obj.PPackage));
            inputParam.Add("@strPlanFeatures", Convert.ToString(obj.PFeature));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_UserPlansForConfig", inputParam);

            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        [HttpPost]
        public string EditUserPlan(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", Convert.ToInt32(2));
            inputParam.Add("@iUserPlansID", Convert.ToInt32(obj.PlanID));
            inputParam.Add("@strPlanName", Convert.ToString(obj.PlanName));
            inputParam.Add("@strRoleID", Convert.ToString(obj.RoleID));
            inputParam.Add("@dFee", Convert.ToDecimal(obj.Fee));
            inputParam.Add("@fCommission", Convert.ToDecimal(obj.Commission));
            inputParam.Add("@fFixedAmount", Convert.ToDecimal(obj.FixedAmount));
            inputParam.Add("@siValidity", Convert.ToInt32(obj.Validity));
            inputParam.Add("@iStatusID", Convert.ToInt32(obj.statusId));
            inputParam.Add("@strPlanType", Convert.ToString(obj.PlanType));
            inputParam.Add("@strPlanPackage", Convert.ToString(obj.PlanPackage));
            inputParam.Add("@strPlanFeatures", Convert.ToString(obj.PlanFeatures));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_UserPlansForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetUserPlan()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserPlans");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        [HttpPost]
        public string DeleteUserPlan(int UserPlansID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();

            inputParam.Add("@iOperationFlag", Convert.ToInt32(4));
            inputParam.Add("@iUserPlansID", Convert.ToInt32(UserPlansID));
            inputParam.Add("@strPlanName", Convert.ToString(""));
            inputParam.Add("@strRoleID", Convert.ToString(""));
            inputParam.Add("@dFee", Convert.ToDecimal(0));
            inputParam.Add("@fCommission", Convert.ToDecimal(0));
            inputParam.Add("@fFixedAmount", Convert.ToDecimal(0));
            inputParam.Add("@siValidity", Convert.ToInt32(0));
            inputParam.Add("@iStatusID", Convert.ToInt32(0));
            inputParam.Add("@strPlanType", Convert.ToString(""));
            inputParam.Add("@strPlanPackage", Convert.ToString(""));
            inputParam.Add("@strPlanFeatures", Convert.ToString(""));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_UserPlansForConfig", inputParam);

            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion

        #region Plan Assignment
        public ActionResult AddPlanAssignment()
        {
            return View();
        }

        [HttpPost]
        public string AddPlanAssignment(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", Convert.ToInt32(1));
            inputParam.Add("@iTraderPlanHasSignalTypesID", Convert.ToInt32(0));
            inputParam.Add("@iUserPlansID", Convert.ToString(obj.UserPlansID));
            inputParam.Add("@iSignalTypesID", Convert.ToInt32(obj.SignalTypesID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_AssignUserPlansForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        [HttpPost]
        public string EditPlanAssignment(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", Convert.ToInt32(2));
            inputParam.Add("@iTraderPlanHasSignalTypesID", Convert.ToInt32(obj.AID));
            inputParam.Add("@iUserPlansID", Convert.ToString(obj.UserPlansID));
            inputParam.Add("@iSignalTypesID", Convert.ToInt32(obj.SignalTypesID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_AssignUserPlansForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        [HttpPost]
        public string DeleteAssignPlan(int TraderPlanHasSignalTypesID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", Convert.ToInt32(4));
            inputParam.Add("@iTraderPlanHasSignalTypesID", Convert.ToInt32(TraderPlanHasSignalTypesID));
            inputParam.Add("@iUserPlansID", Convert.ToString(0));
            inputParam.Add("@iSignalTypesID", Convert.ToInt32(0));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_AssignUserPlansForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        public string GetAllUserPlanAssignment()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllAssignmentPlan");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion
        #region User Creation
        public ActionResult AddUserAndRole()
        {
            return View();
        }
        public string AddNewUserCreation(string Result)
        {
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(Result);
            UserMgnt u = new UserMgnt();
            string Pwd = u.Encrypt(Convert.ToString(obj.Password));
            string hostName = Dns.GetHostName();
            string ipAddress = Dns.GetHostByName(hostName).AddressList[0].ToString();
            inputParam.Add("@strUserEmail", Convert.ToString(obj.EmailId));
            inputParam.Add("@strUserPassword", Convert.ToString(Pwd));
            inputParam.Add("@iUserOTP", Convert.ToString(""));
            inputParam.Add("@iRoleID", Convert.ToInt32(obj.RoleID));
            inputParam.Add("@strUserIPAddresses", Convert.ToString(ipAddress));
            inputParam.Add("@bUserIsActive", Convert.ToByte(obj.StatusID));
            inputParam.Add("@iAdminUserID", globalVariables.SecUserID);
            var dt = sqlDataAccess.ExecuteStoreProcedure("usp_Admin_sec_RegisterUser", inputParam);

            return "";

        }

        public string EditUserRoles(int ID, int RoleID, int UserTypeID, int Status)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iUserID", ID);
            inputParam.Add("@iRoleID", RoleID);
            inputParam.Add("@iUserTypeId", UserTypeID);
            inputParam.Add("@iUserIsActive", Status);
            string str = sqlDataAccess.ExecuteStoreProcedure("usp_admin_UpdateUserRoleByAdmin", inputParam);
            return str;

        }
        public string GetUsersDetails()
        {
            Hashtable inputParam = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParam.Add("@iUserID", globalVariables.SecUserID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Admin_GetUserListing", inputParam);
            string str = JsonConvert.SerializeObject(dt);

            return str;
        }
        #endregion

        #region  Role Allication
        public ActionResult RoleAllocation()
        {
            return View();
        }
        #endregion


        #region Promo Coupon
        public ActionResult Addpromocode()
        {
            return View();
        }

        [HttpPost]
        public string AddPromoCode(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", Convert.ToInt32(1));
            inputParam.Add("@iPromoCouponID", Convert.ToInt32(0));
            inputParam.Add("@strCouponCode", Convert.ToString(obj.CouponCode));
            inputParam.Add("@strDetails", Convert.ToString(obj.Details));
            inputParam.Add("@iRoleID ", Convert.ToInt32(obj.RoleID));
            inputParam.Add("@dDiscountRate", Convert.ToDecimal(obj.DiscountRate));
            inputParam.Add("@sdtStartFrom", Convert.ToDateTime(obj.StartFrom));
            inputParam.Add("@sdtValidTill", Convert.ToDateTime(obj.ValidTill));
            inputParam.Add("@iStatusID", Convert.ToInt32(obj.StatusId));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_PromoCouponForConfig", inputParam);

            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        [HttpPost]
        public string EditPromoCode(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", Convert.ToInt32(2));
            inputParam.Add("@iPromoCouponID", Convert.ToInt32(obj.PromoCouponID));
            inputParam.Add("@strCouponCode", Convert.ToString(obj.CouponCode));
            inputParam.Add("@strDetails", Convert.ToString(obj.Details));
            inputParam.Add("@iRoleID ", Convert.ToInt32(obj.RoleID));
            inputParam.Add("@dDiscountRate", Convert.ToDecimal(obj.DiscountRate));
            inputParam.Add("@sdtStartFrom", Convert.ToDateTime(obj.StartFrom));
            inputParam.Add("@sdtValidTill", Convert.ToDateTime(obj.ValidTill));
            inputParam.Add("@iStatusID", Convert.ToInt32(obj.StatusId));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_PromoCouponForConfig", inputParam);

            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        public string DeletePromoCode(int PromoCouponID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            //dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", Convert.ToInt32(4));
            inputParam.Add("@iPromoCouponID", Convert.ToInt32(PromoCouponID));
            inputParam.Add("@strCouponCode", Convert.ToString(""));
            inputParam.Add("@strDetails", Convert.ToString(""));
            inputParam.Add("@iRoleID ", Convert.ToInt32(0));
            inputParam.Add("@dDiscountRate", Convert.ToDecimal(0));
            inputParam.Add("@sdtStartFrom", Convert.ToDateTime(DateTime.UtcNow));
            inputParam.Add("@sdtValidTill", Convert.ToDateTime(DateTime.UtcNow));
            inputParam.Add("@iStatusID", Convert.ToInt32(0));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_PromoCouponForConfig", inputParam);

            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetAllPromoCode()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPromoCoupon");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion
        #region Message Library
        public ActionResult AddMessagelibrary()
        {
            return View();
        }
        [HttpPost]
        public string AddMessageLibrary(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", Convert.ToInt32(1));
            inputParam.Add("@iMessageLibraryID", Convert.ToInt32(0));
            inputParam.Add("@iChannelID", Convert.ToInt32(obj.ChannelID));
            inputParam.Add("@strMessageName", Convert.ToString(obj.MessageName));
            inputParam.Add("@strMessage", Convert.ToString(obj.Message));
            inputParam.Add("@strMessageType", Convert.ToString(obj.MessageType));
            inputParam.Add("@iSequenceNo", Convert.ToInt32(obj.SequenceNo));
            inputParam.Add("@strRoleID", Convert.ToInt32(obj.RoleID));
            inputParam.Add("@iStatusID", Convert.ToInt32(obj.StatusId));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_MessageLibraryForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        [HttpPost]
        public string EditMessageLibrary(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", Convert.ToInt32(2));
            inputParam.Add("@iMessageLibraryID", Convert.ToInt32(obj.MessageLibraryID));
            inputParam.Add("@iChannelID", Convert.ToInt32(obj.ChannelID));
            inputParam.Add("@strMessageName", Convert.ToString(obj.MessageName));
            inputParam.Add("@strMessage", Convert.ToString(obj.Message));
            inputParam.Add("@strMessageType", Convert.ToString(obj.MessageType));
            inputParam.Add("@iSequenceNo", Convert.ToInt32(obj.SequenceNo));
            inputParam.Add("@strRoleID", Convert.ToInt32(obj.RoleID));
            inputParam.Add("@iStatusID", Convert.ToInt32(obj.StatusId));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_MessageLibraryForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetMessageLibrary()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetMessageLibrary");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        [HttpPost]
        public string DeleteMessageLibrary(int MessageLibraryID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            //  dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", Convert.ToInt32(4));
            inputParam.Add("@iMessageLibraryID", Convert.ToInt32(MessageLibraryID));
            inputParam.Add("@iChannelID", Convert.ToInt32(0));
            inputParam.Add("@strMessageName", Convert.ToString(""));
            inputParam.Add("@strMessage", Convert.ToString(""));
            inputParam.Add("@strMessageType", Convert.ToString(""));
            inputParam.Add("@iSequenceNo", Convert.ToInt32(0));
            inputParam.Add("@strRoleID", Convert.ToInt32(0));
            inputParam.Add("@iStatusID", Convert.ToInt32(0));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_delete_MessageLibraryForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion
        #endregion
        #endregion
        #region CurrentAction
        #region admin-ca-documents
        public ActionResult Documents()
        {

            return View();
        }
        public string DocumentApprove(string SelectedDocId)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@strUserDocumentsID", Convert.ToString(SelectedDocId));
            inputParam.Add("@bIsApproved", Convert.ToBoolean(1));
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_admin_UpdateDocumentStatusByAdmin", inputParam);

            return "";
        }

        public string DocumentReject(string SelectedDocId)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@strUserDocumentsID", Convert.ToString(SelectedDocId));
            inputParam.Add("@bIsApproved", Convert.ToBoolean(0));
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_admin_UpdateDocumentStatusByAdmin", inputParam);

            return "";
        }
        public string GetAdminDocumentList()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_admin_GetDocumentList");
            string str = JsonConvert.SerializeObject(dt);
            // return Json(new { success = true, response = str }, JsonRequestBehavior.AllowGet);
            return str;

        }
        public ActionResult GetUserDocument()
        {
            try
            {
                inputParams = new Hashtable();
                inputParams.Add("@iUserID", globalVariables.SecUserID);
                sqlDataAccess = new SQLDataAccess();
                String[] DocArray = new String[7];
                var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserDocumentDetails", inputParams);
                DocArray[0] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserPhoto"]);
                DocArray[1] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserOnePageProfile"]);
                DocArray[2] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserIdentity"]);
                DocArray[3] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserAddressProof"]);
                DocArray[4] = dt.Rows[0]["UserBankDetails"].ToString();
                DocArray[5] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserBankChequeStatement"]);
                DocArray[6] = dt.Rows[0]["AdminComments"].ToString();
                // string str = JsonConvert.SerializeObject(DocArray);
                return Json(new { success = true, response = DocArray }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = true, response = "Fail" }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion
        #region admin-ca-cashoutprocess
        public ActionResult CashoutProcess()
        {

            return View();
        }
        public string SearchCashoutProcessList(string Result)
        {
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(Result);
            inputParam.Add("@strUserName", Convert.ToString(obj.UserName));
            inputParam.Add("@sdtFromDate", Convert.ToDateTime(obj.FromDate));
            inputParam.Add("@sdtToDate", Convert.ToDateTime(obj.ToDate));
            inputParam.Add("@iRoleID", Convert.ToInt32(obj.RoleID));
            inputParam.Add("@iPaymentStatusID", Convert.ToInt32(obj.PaymentStatusID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_admin_search_GetCashoutRequests", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        [HttpPost]
        public string AddCashoutDetails(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iUserCashoutRequestID", Convert.ToInt32(obj.RequestID));
            inputParam.Add("@iPaymentSourceID", Convert.ToInt32(obj.PaymentSourceID));
            inputParam.Add("@iPaymentStatusID", Convert.ToInt32(obj.PaymentStatus));
            inputParam.Add("@strTransactionID ", Convert.ToString(obj.TrasctionId));
            inputParam.Add("@dtTransactionDate", Convert.ToDateTime(obj.TrasctionDate));
            inputParam.Add("@strComments", Convert.ToString(obj.comment));
            inputParam.Add("@strUserEmail", Convert.ToString(globalVariables.ApplicationUserEmailID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_admin_UpdateCashoutRequestsByAdmin", inputParam);

            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetAllCashoutProcessList()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_admin_GetCashoutRequests");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetBankDetailsByID(int RequestID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@intCashoutRequestID", Convert.ToInt32(RequestID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_admin_GetUserBankDetailsForCashout", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public ActionResult GetPaymentSourceList()
        {
            List<mstPaymentSource> PaymentSourceList = new List<mstPaymentSource>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPaymentSourceList");
            foreach (DataRow r in dt.Rows)
            {
                PaymentSourceList.Add(new mstPaymentSource
                {
                    paymentSourcesID = Convert.ToInt32(r["paymentSourcesID"]),
                    Name = r["Name"].ToString()
                });
            }
            return Json(PaymentSourceList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPaymentStatusList()
        {
            List<mstPaymentStatu> PaymentStatusList = new List<mstPaymentStatu>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPaymentStatusList");
            foreach (DataRow r in dt.Rows)
            {
                PaymentStatusList.Add(new mstPaymentStatu
                {
                    paymentStatusID = Convert.ToInt32(r["paymentStatusID"]),
                    PaymentStatus = r["PaymentStatus"].ToString()
                });
            }
            return Json(PaymentStatusList, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region admin-ca-paymentgateway
        public ActionResult PaymentGateway()
        {
            return View();
        }
        public string GetAllPaymentGetewayList()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_admin_GetPaymentGatewayTransactionList");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string SearchPaymentGeteway(string Result)
        {
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(Result);
            inputParam.Add("@strUserName", Convert.ToString(obj.UserName));
            inputParam.Add("@iPaymentStatusID", Convert.ToInt32(obj.PaymentStatusID));
            inputParam.Add("@sdtFromDate", Convert.ToDateTime(obj.FromDate));
            inputParam.Add("@sdtToDate", Convert.ToDateTime(obj.ToDate));
            inputParam.Add("@iRoleID", Convert.ToInt32(obj.RoleID));
            inputParam.Add("@iPaymentDetailsID", Convert.ToInt32(obj.PaymentSourceID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_admin_search_GetPaymentGatewayTransactionList", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;

        }

        #endregion
        #region admin-ca-ticketmanagement
        public ActionResult TicketManagement()
        {

            return View();
        }
        public string GetTicketManagementData()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Admin_GetAllTicketList");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }       
        public string CloseTicketManagemanet(string SelectedTicket)
        {
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@strTicketID", SelectedTicket);
            string dt = sqlDataAccess.ExecuteStoreProcedure("usp_admin_UpdateTicketStatusByAdmin", inputParam);
            return "";
        }
        public string GetResponseByTicket(int TicketsID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iTicketID", Convert.ToInt32(TicketsID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTicketResponsesByAdmin", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string AddTicketReplay(int TicketsID, string Message)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iTicketID", Convert.ToInt32(TicketsID));
            inputParam.Add("@strMessage", Convert.ToString(Message));
            inputParam.Add("@iTicketStatusID", Convert.ToInt32(1));
            inputParam.Add("@iResponsebyID", Convert.ToInt32(globalVariables.SecUserID));
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_SaveTicketResponses", inputParam);
            return res;
        }
        public ActionResult GetTicketImageById(int TicketID)
        {
            try
            {
                inputParams = new Hashtable();
                inputParams.Add("@iID", TicketID);
                inputParams.Add("@strScreen", "Ticket");
                sqlDataAccess = new SQLDataAccess();
                var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserFile", inputParams);
                string DocArray = Encoding.ASCII.GetString((byte[])dt.Rows[0]["ChooseScreen"]);
                return Json(new { success = true, TicketImage = DocArray }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = true, TicketImage = "Fail" }, JsonRequestBehavior.AllowGet);
            }

        }
        public string SearchTickets(string Result)
        {
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(Result);
            inputParam.Add("@strUserName", Convert.ToString(obj.UserName));
            inputParam.Add("@sdtFromDate", Convert.ToDateTime(obj.FromDate));
            inputParam.Add("@sdtToDate", Convert.ToDateTime(obj.ToDate));
            inputParam.Add("@iRoleID", Convert.ToInt32(obj.RoleID));
            inputParam.Add("@iTicketStatusID", Convert.ToInt32(obj.TicketStatusID));
            inputParam.Add("@iCountryID", Convert.ToInt32(obj.CountryID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_admin_search_GetTicketList", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion
        #region admin-ca-Feedback
        public ActionResult Feedback()
        {
            return View();
        }
        public ActionResult GetImageById(int FeedbackID)
        {
            try
            {
                inputParams = new Hashtable();
                inputParams.Add("@iID", FeedbackID);
                inputParams.Add("@strScreen", "Feedback");
                sqlDataAccess = new SQLDataAccess();
                var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserFile", inputParams);
                string DocArray = Encoding.ASCII.GetString((byte[])dt.Rows[0]["FeedbackFile"]);
                return Json(new { success = true, FeedBackImage = DocArray }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = true, FeedBackImage = "Fail" }, JsonRequestBehavior.AllowGet);
            }

        }
        public string GetAllFeedbackList()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_admin_GetFeedbackList");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
         public string GetMessagesByFeedbackId(int FeedbackID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParams = new Hashtable();
            inputParams.Add("@iFeedbackID", FeedbackID);
            var dt = sqlDataAccess.ExecuteStoreProcedure("usp_GetFeedbackByFeedBackID",inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion
        #region admin-ca-planrenewal
        public ActionResult PlanRenewal()
        {

            return View();
        }
        #endregion
        #region admin-ca-livefeeddelayed
        public ActionResult LiveFeedDelayed()
        {
            // await GetLiveData();                       
            return View();
        }
        public JsonResult GetApiProviderList()
        {
            List<mstLiveFeedSource> LiveFeedList = new List<mstLiveFeedSource>();
            sqlDataAccess = new SQLDataAccess();
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_admin_GetLiveFeedSourceList");

            foreach (DataRow dr in result.Rows)
            {
                LiveFeedList.Add(new mstLiveFeedSource
                {
                    LiveFeedSourceID = Convert.ToInt32(dr["LiveFeedSourceID"]),
                    LiveFeedSourceName = dr["LiveFeedSourceName"].ToString()
                });
            }
            return Json(LiveFeedList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Get()
        {
            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SignalATMEntitiesNew"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(@"SELECT m.MarketName, i.InstrumentName,i.LiveFeedSourceName,convert(VARCHAR, i.EntryDate)AS EntryDate,Convert(VARCHAR,DATEDIFF(mi, i.EntryDate, GETUTCDATE())) AS DelayTime,i.LiveFeed,i.PriceBid,i.PriceAsk,i.PriceMid,inst.DecimalCount FROM InstrumentPriceLive as i INNER JOIN mstInstruments as inst ON i.InstrumentID = inst.InstrumentID  INNER JOIN mstMarkets as m ON m.MarketID = inst.MarketId", connection))
                {

                    // Make sure the command object does not already have
                    // a notification object associated with it.
                    command.Notification = null;

                    //SqlDependency dependency = new SqlDependency(command);
                    //dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    SqlDataReader reader = command.ExecuteReader();

                    var listCus = reader.Cast<IDataRecord>()
                            .Select(x => new
                            {
                                MarketName = (string)x["MarketName"],
                                InstrumentName = (string)x["InstrumentName"],
                                LiveFeedSourceName = (string)x["LiveFeedSourceName"],
                                EntryDate = (string)x["EntryDate"],
                                DelayTime = (string)x["DelayTime"],
                                LiveFeed = (bool)x["LiveFeed"],
                                PriceBid = (decimal)x["PriceBid"],
                                PriceAsk = (decimal)x["PriceAsk"],
                                PriceMid = (decimal)x["PriceMid"],
                               // DecimalCount = (int)x["DecimalCount"]
                            }).ToList();

                    return Json(new { listCus = listCus }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        //private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        //{
        //   CusHub.Show();
        //}
        public string GetLiveDataFeed()
        {
            //usp_Get_AdminLiveFeedDelayed        
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_AdminLiveFeedDelayed");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetLiveDataFeed(int MarketID, int InstrumentId, string SourceName, int MarketStatusId)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iMarketID", MarketID);
            inputParam.Add("@iInstrumentID", InstrumentId);
            inputParam.Add("@strLiveFeedSourceName", SourceName);
            inputParam.Add("@iMarketStatusID", MarketStatusId);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_get_search_AdminLiveFeedDelayed", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string SearchAllFeedbackList(string Result)
        {
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(Result);
            inputParam.Add("@strUserName", Convert.ToString(obj.UserName));
            inputParam.Add("@sdtFromDate", Convert.ToDateTime(obj.FromDate));
            inputParam.Add("@sdtToDate", Convert.ToDateTime(obj.Todate));
            inputParam.Add("@iRoleID", Convert.ToInt32(obj.RoleID));
            inputParam.Add("@strFeedbackSubject", Convert.ToString(obj.FeedBackName));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_admin_search_GetFeedbackList", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        private static System.Timers.Timer aTimer;
        public ActionResult GetLiveData()
        {
            aTimer = new System.Timers.Timer(10000);
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = 3000;
            aTimer.Enabled = true;
            return View();
        }
        private async void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            await GetStartTrueFXData();
        }
        private async Task<ActionResult> GetStartTrueFXData()
        {
            HttpClient client;
            string url = "https://webrates.truefx.com/rates/connect.html?f=csv";
            client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage responseMessage = await client.GetAsync(url);
            if (responseMessage.IsSuccessStatusCode)
            {
                string responseData = responseMessage.Content.ReadAsStringAsync().Result;
                InsertDataIntoDB(responseData);
            }
            return View();
        }

        private void InsertDataIntoDB(string responseData)
        {
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            DataTable mstInstrumentDT = new DataTable();
            DataTable dt2 = new DataTable();
            try
            {
                if (responseData != null)
                {
                    string[] pairsArray = responseData.Split(new Char[] { '\n' });
                    for (int i = 0; i < pairsArray.Length; i++)
                    {
                        string pairList = pairsArray[i].Replace("/", "-");
                        string[] pairData = Regex.Split(pairList, ",");
                        string instrumentName = pairData[0];
                        int resultIns = InsertInstrumentName(instrumentName);
                        if (resultIns == 1)
                        {
                            int instrumentId = GetInstrumentId(instrumentName);
                            inputParams.Clear();
                            if (pairsArray[i] != "\r" && pairsArray[i] != "" && instrumentId != 0)
                            {

                                decimal PriceBid = Convert.ToDecimal(pairData[2] + pairData[3]);
                                decimal PriceAsk = Convert.ToDecimal(pairData[4] + pairData[5]);
                                decimal PriceMid = Convert.ToDecimal((PriceBid + PriceAsk) / 2);
                                DateTime EntryDate = DateTime.Now;

                                inputParams.Add("@InstrumentId", instrumentId);
                                inputParams.Add("@InstrumentName", instrumentName);
                                inputParams.Add("@PriceBid", PriceBid);
                                inputParams.Add("@PriceAsk", PriceAsk);
                                inputParams.Add("@PriceMid", PriceMid);
                                inputParams.Add("@LiveFeed", "True");
                                inputParams.Add("@LiveFeedSourceName", "TrueFX");
                                inputParams.Add("@EntryDate", EntryDate);

                                sqlDataAccess.ExecuteStoreProcedure("usp_Set_InstrumentPriceLive", inputParams);
                                inputParams.Clear();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.Message;
            }
        }

        private int GetInstrumentId(string instrumentName)
        {
            int instrumnetId = 0;
            DataTable mstInstrumentDT = new DataTable();
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            inputParams.Add("@InstrumentName", instrumentName);
            try
            {

                var instrId = sqlDataAccess.GetDatatableExecuteStoreProcedure("[usp_Get_InstrumentId]", inputParams);
                instrumnetId = instrId.Rows[0].Field<int>(0); ;

            }
            catch (Exception ex)
            {

                throw;
            }
            return instrumnetId;
        }
        private int InsertInstrumentName(string instrumentName)
        {
            try
            {
                inputParams = new Hashtable();
                inputParams.Add("@InstrumentName", instrumentName);
                inputParams.Add("@EntryDate", DateTime.Now.ToString());
                sqlDataAccess.ExecuteStoreProcedure("usp_Set_InstrumentName", inputParams);
                inputParams.Clear();
                return 1;
            }
            catch (Exception EX)
            {
                return 0;
            }
            throw new NotImplementedException();
        }
        #endregion
        #region My Signals
        public ActionResult Workspace()
        {
            Hashtable inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            string userName = Session["UserId"].ToString();
            // string userName = "admin@gmail.com"; //Session["UserId"].ToString();
            //     Session["UserId"] = "admin@gmail.com";//temp
            inputParams.Add("@strApplicationUserLoginName", userName);
            var userId = sqlDataAccess.ExecuteStoreProcedure("usp_GetApplicationUserID", inputParams);
            ViewBag.UserID = userId;
            return View();
        }
        public string GetSignalLibrary()
        {
            string str = "";
            inputParams = new Hashtable();
            inputParams.Add("@SigProID",globalVariables.SecUserID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SP_SignalLibrary", inputParams);
            if (dt.Rows.Count==0)
            {
                str = "0";
            }
            else
            {
                str = JsonConvert.SerializeObject(dt);
            }             
            return str; 
        }
        public string GetSignalDashbord()
        {
            string str = "";
            inputParams = new Hashtable();
            inputParams.Add("@SigProID", globalVariables.SecUserID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalDashboard", inputParams);
            if (dt.Rows.Count == 0)
            {
                str = "0";
            }
            else
            {
                str = JsonConvert.SerializeObject(dt);
            }
            return str;
        }
        public string GetClosedSignals()
        {
            string str = "";
            inputParams = new Hashtable();
            inputParams.Add("@SigProID", globalVariables.SecUserID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalClosed", inputParams);
            if (dt.Rows.Count == 0)
            {
                str = "0";
            }
            else
            {
                str = JsonConvert.SerializeObject(dt);
            }
            return str;
        }
        public string GetTimeFrameString(int minTimeFrameId, int maxTimeFrameId)
        {
            inputParams = new Hashtable();
            inputParams.Add("@minTimeFrameID", minTimeFrameId);
            inputParams.Add("@maxTimeFrameID", maxTimeFrameId);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_TimeFramePeriodUnits", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetPinnedCount(int SigID)
        {
            string str = "";
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@SigID", SigID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_PinnedCountForSignal", inputParam);
            if (dt.Rows.Count == 0)
            {
                str = "0";
            }
            else
            {
                str = JsonConvert.SerializeObject(dt);
            }
            return str;
        }
        public string GetViewSignalCount(int SigID)
        {
            string str = "";
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@SigID", SigID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ViewCountForSignal", inputParam);
            if (dt.Rows.Count == 0)
            {
                str = "0";
            }
            else
            {
                str = JsonConvert.SerializeObject(dt);
            }
            return str;
        }
        public string GetSignalProviderInstrumentSuccessRate(int sigProID, int instrumentID)
        {
            string str = "";
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@SigProID", sigProID);
            inputParam.Add("@InstrumentID", instrumentID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalProviderInstrumentSuccessRate", inputParam);
            if (dt.Rows.Count == 0)
            {
                str = "0";
            }
            else
            {
                str = JsonConvert.SerializeObject(dt);
            }
            return str;
        }
        public string GetSignalDetails(int signalsID)
        {
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@SignalsID", signalsID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalDetails", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public int NumberOfTradersForInstrument(int instrumentID)
        {
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@InstrumentID", instrumentID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_NumberOfTradersForInstrument", inputParam);
            int cnt = Convert.ToInt32(dt.Rows[0]["TraderCount"]);
            return cnt;
        }
        public decimal GetMultiplyingFactor(int instrumentID)
        {
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            inputParams.Add("@InstrumentID", instrumentID);
            var applicationUserId = sqlDataAccess.ExecuteStoreProcedure("usp_GetMultiplierFactorByID", inputParams);
            decimal multiplier = Convert.ToDecimal(applicationUserId);
            ViewBag.Multiplier = multiplier;
            return multiplier;
        }
        public string GetSignalProviderActivityPerformanceForPeriod()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@SigProID", globalVariables.SecUserID);
            inputParams.Add("@MarketID", 1);
            inputParams.Add("@PeriodID", 1);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalProviderActivityPerformanceForPeriod", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string SignalProviderActivityPinnedCount()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DateTime startDate = Convert.ToDateTime("2019-12-05 00:00");
            DateTime validTill = DateTime.Now;
            inputParams.Add("@SigProID", globalVariables.SecUserID);
            inputParams.Add("@MarketID", 1);
            inputParams.Add("@StartFrom", startDate);
            inputParams.Add("@ValidTill", validTill);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalProviderActivityPinnedCount", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string SignalProviderActivityProcessStatusCount()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DateTime startDate = Convert.ToDateTime("2019-12-05 00:00");
            DateTime validTill = DateTime.Now;
            inputParams.Add("@SigProID", 240);
            inputParams.Add("@MarketID", 1);
            inputParams.Add("@StartFrom", startDate);
            inputParams.Add("@ValidTill", validTill);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalProviderActivityProcessStatusCount", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public ActionResult Performance()
        {
            return View();
        }
        public ActionResult Preferences()
        {
            return View();
        }
        #endregion


        #region admin-ca-marketwise
        public ActionResult Marketwise()
        {

            return View();
        }
         public  string GetInstrumentLivePrice()
        {           
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_admin_GetMarketWiseInstrumentPriceLive");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string UpdateMarketLiveFeed(int MarketID , int InstrumentID, int LiveFeed)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iMarketID", MarketID);
            inputParam.Add("@iInstrumentID", InstrumentID);
            inputParam.Add("@bLiveFeed",Convert.ToBoolean(LiveFeed));
            var dt = sqlDataAccess.ExecuteStoreProcedure("usp_admin_UpdateInstrumentPriceLiveByAdmin", inputParam);
            return dt;
        }
        public string  UpdateSignalProcessMovement(int SwitchID, string Comments)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iSwitchID",Convert.ToInt32(SwitchID));
            inputParam.Add("@strComments",Convert.ToString(Comments));
            var dt = sqlDataAccess.ExecuteStoreProcedure("usp_admin_UpdateProcessSwitchByAdmin", inputParam);
            return dt;
        }
        #endregion
        #region admin-ca-earningsstatus
        public ActionResult EarningsStatus()
        {          
            return View();
        }
        public string GetEarningStatusList()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            var dataTable = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetEarningStatusList");
            string str = JsonConvert.SerializeObject(dataTable);
            return str;
        }
        #endregion
        #region admin-ca-broadcastmessage
        public ActionResult BroadcastMessage()
        {

            return View();
        }

        [HttpPost]
        public string AddBroadcastMessage(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParam.Add("@iOperationFlag", 1);
            inputParam.Add("@iBroadcastMessageID", 0);
            inputParam.Add("@iMessageTypeID", Convert.ToInt32(obj.MessageTypeID));
            inputParam.Add("@strMessageName", Convert.ToString(obj.MessageName));
            inputParam.Add("@iSequence", Convert.ToInt32(obj.sequenceNo));
            inputParam.Add("@strMessage", Convert.ToString(obj.Message));
            inputParam.Add("@iRoleID", Convert.ToInt32(obj.RoleID));
            inputParam.Add("@iStatusID", Convert.ToInt32(obj.StatusId));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_BroadcastMessageForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;

        }
        [HttpPost]
        public string EditBroadcastMessage(int BroadcastMessageID, string Message, int StatusId)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iOperationFlag", 2);
            inputParam.Add("@iBroadcastMessageID", BroadcastMessageID);
            inputParam.Add("@iMessageTypeID", Convert.ToInt32(0));
            inputParam.Add("@strMessageName", Convert.ToString(""));
            inputParam.Add("@iSequence", Convert.ToInt32(1));
            inputParam.Add("@strMessage", Convert.ToString(Message));
            inputParam.Add("@iRoleID", Convert.ToInt32(0));
            inputParam.Add("@iStatusID", Convert.ToInt32(StatusId));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_insert_update_BroadcastMessageForConfig", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;

        }
        public string GetBrodCastMessages()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetBroadcastMessage");
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion
        #endregion
        #region Monitoring
        #region admin-m-market
        public ActionResult MonitoringMarket()
        {
            return View();
        }
        #endregion
        #region admin-m-user
        public ActionResult MonitoringUser()
        {
            return View();
        }
        #endregion
        #region admin-m-Payment
        public ActionResult MonitoringPayment()
        {
            return View();
        }
        #endregion
        #region admin-m-graph
        public ActionResult MonitoringGraphs()
        {
            return View();
        }
        #endregion
        #region admin-m-Performance
        public ActionResult MonitoringPerformance()
        {
            return View();
        }
        #endregion
        #region admin-m-plan-movement
        public ActionResult MonitoringPlanMovement()
        {
            return View();
        }
        #endregion
        #endregion
        public ActionResult SiteMap()
        {
            return View();
        }
        #region Success Story

        public ActionResult SuccessStory()
        {
            return View();
        }
        public string GetReferralList()
        {
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_ReferrerSuccessStory");
            string str = JsonConvert.SerializeObject(dt);
            return str;

        }
        public string SearchSuccessStorySignalProvider(int MarketID)
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iMarketID", MarketID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SignalProviderSuccessStory", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetSuccessStoryTrader(int MarketID)
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iMarketID", MarketID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_TradersSuccessStory", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }


        public string SearchSuccessStoryScript(int MarketID)
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iMarketID", MarketID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_ScriptsSuccessStory", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion 
    }
}