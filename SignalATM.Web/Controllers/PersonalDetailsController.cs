﻿using CsQuery.ExtensionMethods.Internal;
using SignalATM.Commons;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SignalATM.Web.Controllers
{
    public class PersonalDetailsController : Controller
    {
        // GET: PersonalDetails
        public GlobalVariables globalVariables = new GlobalVariables();
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult GetCountry()
        {            
            try
            {             
                var dataTableResult = SqlCommons.ExecuteStoreProcedure(globalVariables.SignalATMDbConn, StoredProcedures.GetCountry);
                return Json(new { success = false, result = dataTableResult }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Debug.logError(string.Format("Exception occured in Home.VerifyOTPToRegister. Ex:- {0}", ex.Message));
                return Json(new { success = false, result = "fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetAllStateByCountryID(int countryID)
        {           
            try
            {
                Hashtable hInput = new Hashtable();
                hInput.Add("@CountryID", countryID);
                DataTable queryResult = SqlCommons.ExecuteStoreProcedure(globalVariables.SignalATMDbConn, StoredProcedures.GetAllStateByCountryID, hInput);

                return Json(new { success = false, result = queryResult }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Debug.logError(string.Format("Exception occured in Home.VerifyOTPToRegister. Ex:- {0}", ex.Message));
                return Json(new { success = false, result = "fail" }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}