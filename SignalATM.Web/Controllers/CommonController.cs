﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SignalATM.Web.Controllers
{
    public class CommonController : Controller
    {
        // GET: Common
        public ActionResult Index()
        {
            return View();
        }
        #region Personal details
        public ActionResult PersonalDetails()
        {
            return View();
        }
        #endregion
        #region Documents
        public ActionResult Documents()
        {
            return View();
        }
        #endregion
        #region GlobalProfile
        public ActionResult GlobalProfile()
        {
            return View();
        }
        #endregion

    }
}