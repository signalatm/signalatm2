﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using InfoFreightSystem;
using SignalATM.Web.Models;
using System.Data;
using SignalATM.Commons;
using Microsoft.Ajax.Utilities;
using CsQuery.ExtensionMethods.Internal;
using System.Data.SqlClient;
using Microsoft.AspNet.SignalR.Messaging;

namespace SignalATM.Web.Controllers
{
    //[RequireHttps]
    public class HomeController : Controller
    {        
        

        Userclass userAllInfo = new Userclass();
        SQLDataAccess sqlDataAccess;
        Hashtable inputParams;
        public GlobalVariables globalVariables = new GlobalVariables();

       public ActionResult Login(string emailId)
       {
            ViewBag.Message = "Your application description page.";
            return View();
        }        
       
        public ActionResult LoginDetails(string email, string password, string otp)
        {
            UserMgnt u = new UserMgnt();
            string pwd = u.Encrypt(password);
           // Session["UserId"] = email;

            Hashtable hInput = new Hashtable();
            hInput.Add("@strEmailAddress", email);
            hInput.Add("@password", pwd);
            hInput.Add("@strOTP", otp);
            DataTable queryResult = SqlCommons.ExecuteStoreProcedure(globalVariables.SignalATMDbConn, StoredProcedures.UserLoginVerify, hInput);
            string result = queryResult.Rows[0]["Result"].ToString();            
            int roleid = Convert.ToInt32(queryResult.Rows[0]["RoleID"]);

            if(result == "True" && roleid!=0)
            {
                string commandText = "Select UserID from secUsers Where UserEmailID=@email";
                int userId = 0;
                SqlCommand cmd = new SqlCommand(commandText, globalVariables.SignalATMDbConn);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@email";
                param.Value = email;
                cmd.Parameters.Add(param);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    
                      userId= Convert.ToInt32( reader["UserID"]);
                }

                GlobalVariables obj = new GlobalVariables(userId);
                //Hashtable input = new Hashtable();
                //input.Add("@strEmailAddress", email);
                //input.Add("@roleid", roleid);
                
                //DataTable temp = SqlCommons.ExecuteStoreProcedure(globalVariables.SignalATMDbConn, StoredProcedures.RegisterUserRoleSelection, input);
                return Json(new { success = true, result = "PersonalDetails" }, JsonRequestBehavior.AllowGet);
            }
            return View();
        }
        //public JsonResult LoginDetailsAsync(string email, string password)
        //{
        //    string result = "Fail";
        //    try
        //    {               
        //        UserMgnt u = new UserMgnt();
        //        string pwd = u.Encrypt(password);
        //        Session["UserId"] = email;

        //        Hashtable hInput = new Hashtable();               
        //        hInput.Add("@strEmailAddress",email);
        //        hInput.Add("@password",pwd);
        //        hInput.Add("@strOTP", "");
        //        string isUserExists = SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.UserLoginVerify, hInput);

        //        if(string.IsNullOrEmpty(isUserExists) || isUserExists == "0")
        //        {
        //            return Json(new { success = true, result = "NotExist" }, JsonRequestBehavior.AllowGet);
        //        }

        //        if(isUserExists == "2")
        //        {                       
        //            var isFirst = IsFirstLogin(email);
        //            if(isFirst == "True")
        //            {
        //                result = "FirstLogin";
        //            }
        //            else
        //            {
        //                string res = IsCountryAndTimezoneExist(email);
        //                if (res == "0")
        //                {
        //                    return Json(new { success = true, result = "BasicConfiguration" }, JsonRequestBehavior.AllowGet);
        //                }
        //                u.Password = u.Encrypt(password);
        //                var request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["WebAPI_URL"].ToString() + "token");
        //                var postData = "grant_type=" + Uri.EscapeDataString(ConfigurationManager.AppSettings["GrantType"].ToString());
        //                postData += "&username=" + Uri.EscapeDataString(email);
        //                //postData += "&password=" + Uri.EscapeDataString(txtPassword.Text);
        //                postData += "&password=" + Uri.EscapeDataString(u.Password);
        //                var data = Encoding.ASCII.GetBytes(postData);
        //                //SetGlobalVariables(email);
        //                globalVariables = new GlobalVariables(email);
        //                request.Method = "POST";
        //                request.ContentType = "application/x-www-form-urlencoded";
        //                request.ContentLength = data.Length;                                                      
        //                using (var stream = request.GetRequestStream())
        //                {
        //                    stream.Write(data, 0, data.Length);
        //                }
        //                var response = (HttpWebResponse)request.GetResponse();
        //                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
        //                if (responseString != "")
        //                {
        //                    JObject jObject = JObject.Parse(responseString);
        //                    string Access_token = (string)jObject.SelectToken("access_token");
        //                    string token_type = (string)jObject.SelectToken("token_type");
        //                    string expires_in = (string)jObject.SelectToken("expires_in");
        //                    string UserName = (string)jObject.SelectToken("UserName");
        //                    System.Web.HttpCookie myCookie = new System.Web.HttpCookie("access_token");
        //                    Session["UserName"] = UserName;
        //                    Session["Access_token"] = Access_token;
        //                    Session["token_type"] = token_type;
        //                    Session["expires_in"] = expires_in;
        //                    int roleId = globalVariables.RoleID;
        //                    return Json(new { success = true, result = "OK" + roleId }, JsonRequestBehavior.AllowGet);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            result = "NotActive";
        //        }

        //        return Json(new { success = true, result = result }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch(Exception ex)
        //    {
        //        return Json(new { success = true, result = ex.Message }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        public ActionResult UserNotActiveMessage()
        {
            return View();
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Login", "Home");
        }
        [HttpGet]
        public ActionResult Register()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }       
        /// <summary>
        /// This action validates OTP against a given email in the process of user registration
        /// </summary>
        /// <param name="email"></param>
        /// <param name="OTP"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult VerifyOTPToRegister(string email, string OTP)
        {
            // RegisterVerifyOTP
            try
            {
                Hashtable reg_User = new Hashtable();
                reg_User.Add("@strEmailAddress", email);
                reg_User.Add("@strOTP", email); 
                 var isSaveOTPSuccess = SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.RegisterVerifyOTP, reg_User);

                if (isSaveOTPSuccess == ExecutionStatus.SUCCESS.GetValue().ToString())
                {
                    Debug.logInfo(string.Format("OTP verification successful. Email:-{0}", email));
                     return Json(new { success = true, result = "Success" }, JsonRequestBehavior.AllowGet);
                   // return RedirectToAction("Home", "SavePassword", new { registrationEmail = email });
                }
                else
                {
                    Debug.logInfo(string.Format("OTP verification failed. Email:-{0}", email));
                    // return Json(new { success = true, result = "OTP save successful." }, JsonRequestBehavior.AllowGet);
                    return Json(new { success = true, result = "Incorrect" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Debug.logError(string.Format("Exception occured in Home.VerifyOTPToRegister. Ex:- {0}", ex.Message));
                return Json(new { success = false, result = "fail" }, JsonRequestBehavior.AllowGet);
            }   
        }

        /// <summary>
        /// This action is executed when user enters Email and click on Register button
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RegisterUser(string email)
        {
             //On click of Register button, email is verified against database. 
             //  If EMail doesn't exists, then displa
             //  Status:- 
             //       2: User registration completed, 
             //       1: User registration started but verification not completed. 
             //       0: Ready to start registration by adding email to db and sending OTP to email
           
            try
            {
                Hashtable reg_User = new Hashtable();
                reg_User.Add("@strEmailAddress", email);
                var registerUserEmail = SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.RegisterUserEmail, reg_User);
                
                // email varification failed
                if (registerUserEmail== RegistrationStatus.INVALID_EMAIL.GetValue().ToString())
                {
                    Debug.logInfo(string.Format("Invalid EMail. Email:-{0}", email));
                    return Json(new { success = true, result = "Invalid email" }, JsonRequestBehavior.AllowGet);
                }
                else if (registerUserEmail == RegistrationStatus.NEW_VAILD_EMAIL.GetValue().ToString())
                {
                    OTP otp = new OTP();

                    this.SaveOTP(email, OTP.GenerateOtp());
                    Debug.logInfo(string.Format("Valid email. Ready for registration. Email:- {0}", email));
                    // return RedirectToAction("Home", "SubmitOTP", new { registrationEmail = email });
                    var result = new { data = email, status = "Success" };
                    return Json( result, JsonRequestBehavior.AllowGet );
                }
                else if (registerUserEmail == RegistrationStatus.ALREADY_REGISTERED.GetValue().ToString())
                {
                    Debug.logInfo(string.Format("Already registered. Email:- {0}", email));
                    return Json(new { success = true, result = "LoginPage" }, JsonRequestBehavior.AllowGet);
                    //return RedirectToAction("Home", "LoginPage", new { registrationEmail = email });
                }
                else
                    return Json(new { success = true, result = "Invalid email" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Debug.logError(string.Format("Exception occured in Home.RegisterUser. Ex:- {0}", ex.Message));
                return Json(new { success = false, result = "fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        //public ActionResult SubmitOTP(string registrationEmail)
        //{
        //    ViewBag.Email = registrationEmail;
        //    return View();
        //}
        [HttpPost]
        public ActionResult RegisterUserEnterPassword(string email, string password)
        {
            // RegisterPassword
            try
            {
                UserMgnt u = new UserMgnt();
                string pwd = u.Encrypt(password);

                Hashtable reg_User = new Hashtable();
                reg_User.Add("@strEmailAddress", email);
                reg_User.Add("@password", pwd);
                var isSavePasswordSuccess = SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.RegisterUserPassword, reg_User);

                //if (isSavePasswordSuccess == ExecutionStatus.SUCCESS.GetValue().ToString())
                if(isSavePasswordSuccess == "True")
                {
                    Debug.logInfo(string.Format("Password save successfully. Email:-{0}", email));
                    var result = new { Status="Success",Email=email};
                    return Json(result, JsonRequestBehavior.AllowGet);
                    // return RedirectToAction("Home", "SavePassword", new { registrationEmail = email });
                }
                else
                {
                    Debug.logInfo(string.Format("Issue while saving password. Email:-{0}", email));
                    // return Json(new { success = true, result = "OTP save successful." }, JsonRequestBehavior.AllowGet);
                    return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Debug.logError(string.Format("Exception occured in Home.RegisterUserEnterPassword. Ex:- {0}", ex.Message));
                return Json(new { success = false, result = "fail" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<JsonResult> RegisterDetails(string email, string password)
        {

            RegisterUser(email);
            string str = "";
            Boolean result = false;
            string sMessage = "Fail";
            try
            {
                OTP otp = new OTP();
                UserMgnt u = new UserMgnt();
                string hostName = Dns.GetHostName();
                string ipAddress = Dns.GetHostByName(hostName).AddressList[0].ToString();
                Hashtable ht_reg = new Hashtable();

                var genOtp = OTP.GenerateOtp();
                u.Password = u.Encrypt(password);

                ht_reg.Add("@strUserEmail", email);
                ht_reg.Add("@strUserPassword", u.Password);
                ht_reg.Add("@iUserOTP", genOtp);
                ht_reg.Add("@iRoleID", 6);
                ht_reg.Add("@strUserIPAddresses", ipAddress);
                ht_reg.Add("@bUserIsActive", 0);
                string strResult = SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.RegisterUser, ht_reg);

                //_SignalATM = new SignalATMClass();
                //string strResult = _SignalATM.Register(ht_reg);


                //Hashtable reg_User = new Hashtable();
                //reg_User.Add("@strApplicationUserEmailID", email);
                //var userId = _SignalATM.GetuserId(reg_User); // where is this user id used ??

                if (strResult != "True")
                {
                    Session["EmailTo"] = email;
                    Session["OTP"] = strResult;
                    ViewBag.OTP = strResult;

                    result = await EmailManager.SendEmailAsync(email, MessageType.RegistrationEmail, genOtp);
                    sMessage = (result == true ? "OK" : "Fail");
                }
            }
            catch (Exception ex)
            {
                Debug.logError(ex);
                sMessage = ex.Message;
            }

            return Json(new { success = result, result = sMessage }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ResendEmail(string email)
        {
            ViewBag.reEmail = email;
            return View();
        }
        public ActionResult SuccessfullRegistrationMsg()
        {
            return View();
        }
        public async Task<ActionResult> SendActivationMail(string email)
        {
            Boolean result = false;
            using (var client = new HttpClient())
            {
                result = await EmailManager.SendEmailAsync(email, MessageType.ActivationEmail,"");
                Hashtable hd = new Hashtable();
                hd.Add("@strApplicationUserLoginName", email);
                SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.UpdateActivationEmailDate, hd);
                //SignalATMClass _SignalATM = new SignalATMClass();
                //_SignalATM.UpdateActivationEmailDate(email);
                if (result)
                {
                    return RedirectToAction("Login", "Home");
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
            }
        }
        public ActionResult ActivateUserAccount()
        {
            string email = Request.QueryString["email"].ToString();
            ViewBag.EmailId = email;
            Hashtable ht_reg = new Hashtable();
            ht_reg.Add("@strApplicationUserLoginName", email);
            sqlDataAccess = new SQLDataAccess();
            string result = sqlDataAccess.ExecuteStoreProcedure("usp_ActiveUser", ht_reg);
            if(result=="0")
            {
                ViewBag.response = "Your account is already activated . Please login with valid account number and password!";
                return View();
            }
            else if(result=="3")
            {
                return RedirectToAction("ResendEmail", "Home",new { email=email });
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
           
        }       
        public ActionResult Forgot_Password()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Forgot_PasswordDetails(string email)   
        {
            return View();      
        }
        public async Task<JsonResult> SaveForgotPassword(string userEmail, string password)
        {
            Boolean returnVal = false;
            string sResult = "";

            if (userEmail != null && password != null)
            {
                UserMgnt u = new UserMgnt();
                u.Password = u.Encrypt(password);
                //SignalATMClass _SignalATM = new SignalATMClass();

                Hashtable hs = new Hashtable();
                hs.Add("@email", userEmail);
                hs.Add("@password", u.Password);

                //string strResult = _SignalATM.UpdatePassword(hs);
                string strResult = SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.UpdatePassword, hs);
                if (strResult == "True")
                {
                    returnVal = await EmailManager.SendEmailAsync(userEmail, MessageType.ForgotPassword, "");
                    sResult = (returnVal == true ? "OK" : "Fail");
                }
                else
                {
                    sResult = "Fail";
                }
            }
            return Json(new { success = returnVal, result = sResult }, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> IsUserExistsPwd(string email)
        {
            Boolean returnVal = false;
            string strResult = "User not exists!";
            try
            {
                Session["UserId"] = email;
                Hashtable hInput = new Hashtable();
                hInput.Add("@iUserEmail", email);
                //SignalATMClass signalATM = new SignalATMClass();
                //string isUserExists = signalATM.CheckEmailAddressExists(hInput);
                string isUserExists = SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.EmailAddressExists, hInput);
                if (isUserExists == "0")
                {
                    if (isUserExists == "1")
                    {
                        returnVal = await EmailManager.SendEmailAsync(email, MessageType.PasswordResetEmail,"");
                        strResult = (returnVal == true ? "OK" : "Fail");
                    }
                    else
                    {
                        strResult = "User is not activated!";
                    }
                }
            }
            catch (Exception ex)
            {
                strResult = ex.Message;
            }
            return Json(new { status = returnVal, result = strResult }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Role_assigned(string emailID)
        {
            var loginWith = string.Empty;
            if (UserMgnt.loginType != "")
            {
                loginWith = UserMgnt.loginType;
            }
            ViewBag.Email = emailID;
            //if (loginWith == "facebook")
            //{
            //    string accessCode = Request.QueryString["code"].ToString();

            //    var fb = new FacebookClient();

            //    // throws OAuthException 
            //    dynamic result = fb.Post("oauth/access_token", new
            //    {
            //        client_id = "1205696766486977",
            //        client_secret = "c1e34106bf083041714176327ff502e0",
            //        redirect_uri = redirect_url,
            //        code = accessCode
            //    });

            //    var accessToken = result.access_token;
            //    var expires = result.expires;

            //    // Store the access token in the session
            //    Session["AccessToken"] = accessToken;

            //    // update the facebook client with the access token 
            //    fb.AccessToken = accessToken;

            //    // Calling Graph API for user info
            //    dynamic me = fb.Get("me?fields=id,name,email,gender,photos,location");
            //    //string id = me.id; // You can store it in the database                
            //    //string email = me.email;
            //    string name = me.name;
            //    string[] authorsList = name.Split(' ');

            //    string firstName = authorsList[0].ToString();
            //    string lastName = authorsList[1].ToString();

            //    otp otp = new otp();
            //    UserMgnt u = new UserMgnt();
            //    var genOtp = otp.GenerateOtp();
            //    SignalATMClass _SignalATM = new SignalATMClass();
            //    Hashtable ht_reg = new Hashtable();
            //    ht_reg.Add("@iUserEmail", me.email);
            //    ht_reg.Add("@iUserPassword", "");
            //    ht_reg.Add("@iUserOTP", genOtp);
            //    ht_reg.Add("@iFirstName", firstName);
            //    ht_reg.Add("@iLastName", lastName);
            //    ht_reg.Add("@iSocialMedia", true);
            //    ht_reg.Add("@iSocialMediaID", me.id);
            //    string strResult = _SignalATM.RegisterBySocialMedia(ht_reg);
            //    //if (strResult == "User has been registed.")
            //    //    return Json(new { success = true, result = "OK" }, JsonRequestBehavior.AllowGet);
            //    //else
            //    //    return Json(new { success = true, result = "Failed" }, JsonRequestBehavior.AllowGet);
            //}

            // if (loginWith == "gmail")
            //{
            //    String Parameters;
            //    ///string loginWith = Session["loginWith"].ToString();
            //    var url = Request.Url.Query;
            //    String queryString = url.ToString();
            //    char[] delimiterChars = { '=' };
            //    string[] words = queryString.Split(delimiterChars);
            //    string code = words[1];
            //    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
            //    webRequest.Method = "POST";
            //    Parameters = "code=" + code + "&client_id=" + googleplus_client_id + "&client_secret=" + googleplus_client_secret + "&redirect_uri=" + redirect_url + "&grant_type=authorization_code";
            //    byte[] byteArray = Encoding.UTF8.GetBytes(Parameters);
            //    webRequest.ContentType = "application/x-www-form-urlencoded";
            //    webRequest.ContentLength = byteArray.Length;
            //    Stream postStream = webRequest.GetRequestStream();
            //    // Add the post data to the web request
            //    postStream.Write(byteArray, 0, byteArray.Length);
            //    postStream.Close();

            //    WebResponse response = webRequest.GetResponse();
            //    postStream = response.GetResponseStream();

            //    StreamReader reader = new StreamReader(postStream);
            //    string responseFromServer = reader.ReadToEnd();

            //    GooglePlusAccessToken serStatus = JsonConvert.DeserializeObject<GooglePlusAccessToken>(responseFromServer);

            //    if (serStatus != null)
            //    {
            //        string accessToken = string.Empty;
            //        accessToken = serStatus.access_token;

            //        if (!string.IsNullOrEmpty(accessToken))
            //        {
            //            // This is where you want to add the code if login is successful.
            //            getgoogleplususerdataSerAsync(accessToken);
            //        }
            //    }
            //}
            return View();
        }

        private async Task getgoogleplususerdataSerAsync(string accessToken)
        {
            try
            {
                HttpClient client = new HttpClient();
                var urlProfile = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + accessToken;

                HttpResponseMessage output = await client.GetAsync(urlProfile);

                if (output.IsSuccessStatusCode)
                {
                    string outputData = await output.Content.ReadAsStringAsync();
                    GoogleUserOutputData serStatus = JsonConvert.DeserializeObject<GoogleUserOutputData>(outputData);

                    if (serStatus != null)
                    {
                        // You will get the user information here.
                        userAllInfo.id = serStatus.id;
                        userAllInfo.first_name = serStatus.given_name;
                        userAllInfo.last_name = serStatus.family_name;
                        userAllInfo.Emails = serStatus.email;
                        userAllInfo.locale = serStatus.locale;
                        userAllInfo.picture = serStatus.picture;
                        userAllInfo.name = serStatus.name;
                        Session["UserName"] = serStatus.name;
                        String str = userAllInfo.name;

                        char[] spearator = { ',', ' ' };
                        // using the method 
                        String[] strlist = str.Split(spearator,
                           StringSplitOptions.RemoveEmptyEntries);

                        foreach (String s in strlist)
                        {
                            // string Firstname = s;
                            //string Lastname = s;
                        }
                        OTP otp = new OTP();
                        UserMgnt u = new UserMgnt();
                        var genOtp = OTP.GenerateOtp();
                        SignalATMClass _SignalATM = new SignalATMClass();
                        Hashtable ht_reg = new Hashtable();
                        ht_reg.Add("@iUserEmail", userAllInfo.Emails);
                        ht_reg.Add("@iUserPassword", "");
                        ht_reg.Add("@iUserOTP", genOtp);
                        ht_reg.Add("@iFirstName", userAllInfo.first_name);
                        ht_reg.Add("@iLastName", userAllInfo.last_name);
                        ht_reg.Add("@iSocialMedia", true);
                        ht_reg.Add("@iSocialMediaID", userAllInfo.id);
                        string strResult = RegisterBySocialMedia(ht_reg);

                        client.CancelPendingRequests();
                    }
                }
                RedirectToAction("Login");
            }
            catch (Exception ex)
            {

            }
        }
       
        public ActionResult Login_otp()
        {
            return View();
        }
        public ActionResult Reset_Password()
        {
            return View();
        }
        public JsonResult Reset_PasswordDetails(string password)
        {
            string strResult = "False";
            if (Session["EmailTo"] != null)
            {
                UserMgnt u = new UserMgnt();
                u.Password = u.Encrypt(password);
                string email = Convert.ToString(Session["EmailTo"].ToString());
                //SignalATMClass _SignalATM = new SignalATMClass();
                Hashtable hs = new Hashtable();
                hs.Add("@email", email);
                hs.Add("@password", u.Password);
                strResult = SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.UpdatePassword, hs);
                //string strResult = _SignalATM.UpdatePassword(hs);
                //if (strResult == "True")
                //{
                //    return Json(new { success = true, status = true }, JsonRequestBehavior.AllowGet);
                //}
                //else
                //{
                //    return Json(new { success = true, status = false }, JsonRequestBehavior.AllowGet);
                //}
            }
            return Json(new { success = true, status = strResult }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult Basic_Configuration()
        {           
            List<mstMarket> marketList  = GetMarketList();           
            List<mstInstrument> instList = GetInstrumentList();
            BasicConfiguarationViewModel model = new BasicConfiguarationViewModel();
            model.MarketList = marketList;
            model.InstrumentList = instList;
            ViewBag.InstList = instList;
            return View(model);
        }    
        
        public static List<mstMarket> GetMarketList()
        {          
            string url = ConfigurationManager.AppSettings["BaseUrl"].ToString() + "serviceproviders/getmarkets/";
           var request = (HttpWebRequest)WebRequest.Create(url);                 
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";            
            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            JObject marketList = new JObject();
            marketList = (JObject)JsonConvert.DeserializeObject(responseString);
            IList<JToken> results = marketList["data"].Children().ToList();
            List<mstMarket> marketResult = new List<mstMarket>();
            foreach (JToken dr in results)
            {
                marketResult.Add(new mstMarket
                {
                    MarketID = Convert.ToInt32(dr["MarketID"]),
                    MarketName = dr["MarketName"].ToString()
                });
            }
            return marketResult;
        }
        public  List<mstInstrument> GetInstrumentList()
        {
            List<mstInstrument> instrumentList = new List<mstInstrument>(); 
            //Hashtable inputParam = new Hashtable();
            //sqlDataAccess = new SQLDataAccess();
            DataTable dt = new DataTable();
            dt = SqlCommons.ExecuteStoreProcedure(globalVariables.SignalATMDbConn, StoredProcedures.GetAllInstruments);
                //sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllInstruments");          
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                 mstInstrument inst = new mstInstrument();
                 inst.InstrumentID = Convert.ToInt32(dt.Rows[i]["InstrumentID"]);
                 inst.InstrumentsName = dt.Rows[i]["InstrumentsName"].ToString();
                inst.MarketId =Convert.ToInt32( dt.Rows[i]["MarketId"]);
                 instrumentList.Add(inst);
            }
            return instrumentList;
        }
        //public static List<mstInstrument> GetInstrumentListByMarketId(int marketId)
        //{
        //         JObject instList = new JObject();
        //        List<mstInstrument> instDataRes = new List<mstInstrument>();

        //        string url = ConfigurationManager.AppSettings["BaseUrl"].ToString() + "GetInstrumentsByMarket?MarketId=";
        //        var request = (HttpWebRequest)WebRequest.Create(url+marketId);
        //        request.Method = "GET";
        //        request.ContentType = "application/x-www-form-urlencoded";
        //        var response = (HttpWebResponse)request.GetResponse();
        //        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();              
        //        instList = (JObject)JsonConvert.DeserializeObject(responseString);
        //        IList<JToken> results = instList["data"].Children().ToList();
        //        foreach (JToken dr in results)
        //        {
        //            instDataRes.Add(new mstInstrument
        //            {
        //                InstrumentID = Convert.ToInt32(dr["InstrumentID"]),
        //                InstrumentsName = dr["InstrumentsName"].ToString()
        //            });
        //        }
        //        return instDataRes;            
        //}
        public async Task<JsonResult> GetInstrumentListByMarketId(int marketId)
        {
            JObject instList = new JObject();
            List<mstInstrument> instDataRes = new List<mstInstrument>();          
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseUrl"].ToString());
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage resp = await client.GetAsync("serviceproviders/GetInstrumentsByMarket?MarketId=" + marketId);

                if (resp.IsSuccessStatusCode)
                {
                    var instResponse = resp.Content.ReadAsStringAsync().Result;
                    instList = (JObject)JsonConvert.DeserializeObject(instResponse);
                    IList<JToken> results = instList["data"].Children().ToList();
                    foreach (JToken dr in results)
                    {
                        instDataRes.Add(new mstInstrument
                        {
                            InstrumentID = Convert.ToInt32(dr["InstrumentID"]),
                            InstrumentsName = dr["InstrumentsName"].ToString()
                        });
                    }
                    return Json(instDataRes, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        [HttpPost]
        public ActionResult Basic_Configuration(int contrId,int timeZoneId,string instArray)
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                string user = Session["UserId"].ToString();
                Hashtable reg_User = new Hashtable();
                reg_User.Add("@strApplicationUserEmailID", user);
                int userId = GetuserId(reg_User);
                Hashtable inputParam = new Hashtable();
                string xmlInstData = GetXml(instArray, userId);
                inputParam.Add("@iCountryID", contrId);
                inputParam.Add("@iTimeZoneID", timeZoneId);
                inputParam.Add("@iUserID", userId);
              //  inputParam.Add("@iMarketID", marketID);
                inputParam.Add("@xmlData", xmlInstData);
                var response = sqlDataAccess.ExecuteStoreProcedure("usp_Save_BasicConfiguration", inputParam);

                //var request = (HttpWebRequest)WebRequest.Create("http://apps.brainlines.net/SignalATMWebAPI/api/serviceproviders/savebasicconfiguration/");
                //string postData = string.Empty;
                //postData += "&iCountryID=" + contrId + "&iTimeZoneID="+ timeZoneId+ "&iUserID=" + userId+ "&iMarketID=" + marketID+ "&xmlData=" + xmlInstData;             

                //var data = Encoding.ASCII.GetBytes(postData);
                //request.Method = "POST";
                //request.ContentType = "application/x-www-form-urlencoded";
                //request.ContentLength = data.Length;
                //using (var stream = request.GetRequestStream())
                //{
                //    stream.Write(data, 0, data.Length);
                //}
                //var response = (HttpWebResponse)request.GetResponse();
                //var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return View("OK");
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }
        public string GetXml(IEnumerable list, int userId)
        {
            Hashtable reg_User = new Hashtable();
            reg_User.Add("@UserId", userId);
            int RoleId = GetuserRoleId(reg_User);
            StringBuilder sbXml = new StringBuilder();
            DateTime entryDate = DateTime.Now.Date;
            sbXml.Append("<InstrumentLists>");
            if (RoleId == 4)
            {
                foreach (object obj in list)
                {
                    sbXml.Append("<InstrumentList>");
                    sbXml.Append("<SigProID>" + userId);
                    // sbXml.Append('"'+ userId + '"');
                    sbXml.Append("</SigProID>");

                    sbXml.Append("<InstrumentID>" + Convert.ToInt32(obj.ToString()));
                    // sbXml.Append('"' + obj.ToString() + '"');                  
                    sbXml.Append("</InstrumentID>");

                    sbXml.Append("<StatusID>");
                    sbXml.Append('"' + 1 + '"');
                    sbXml.Append("</StatusID>");

                    sbXml.Append("<EntryDate>'" + entryDate);
                    // sbXml.Append('"' + DateTime.UtcNow.ToString() + '"');                   
                    sbXml.Append("'</EntryDate>");
                    sbXml.Append("</InstrumentList>");
                }
            }
            else if (RoleId == 2)
            {

                foreach (object obj in list)
                {
                    sbXml.Append("<InstrumentList>");
                    sbXml.Append("<SigProID>" + userId);                                  
                    sbXml.Append("</SigProID>");

                    sbXml.Append("<InstrumentID>" + Convert.ToInt32(obj.ToString()));                                  
                    sbXml.Append("</InstrumentID>");
                    sbXml.Append("</InstrumentList>");
                }
            }
            sbXml.Append("</InstrumentLists>");
            return sbXml.ToString();
        }

        [HttpPost]
        public ActionResult SaveUserRole(string userRole,string userEmail)
        {
            try
            {
                string hostName = Dns.GetHostName();
                string ipAddress = Dns.GetHostByName(hostName).AddressList[0].ToString();

                Hashtable reg_User = new Hashtable(); 
                reg_User.Add("@strEmailAddress", userEmail);
                reg_User.Add("@RoleName", userRole);
                reg_User.Add("@IPAddress", ipAddress);
                var isSaveRoleSuccess = SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.RegisterUserRoleSelection, reg_User);

                // if (isSaveRoleSuccess == ExecutionStatus.SUCCESS.GetValue().ToString())
                if (isSaveRoleSuccess == "True")
                {
                    Debug.logInfo(string.Format("User Role save successfully. Email:-{0}", userEmail));
                    return Json(new { success = true, result = "Success" }, JsonRequestBehavior.AllowGet);
                    // return RedirectToAction("Home", "SavePassword", new { registrationEmail = email });
                }
                else
                {
                    Debug.logInfo(string.Format("Issue while saving role. Email:-{0}", userEmail));
                    // return Json(new { success = true, result = "OTP save successful." }, JsonRequestBehavior.AllowGet);
                    return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Debug.logError(string.Format("Exception occured in Home.SaveUserRole. Ex:- {0}", ex.Message));
                return Json(new { success = false, result = "fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveUserRole1(string userRole)
        {
             //ViewBag.UserRole = userRole;
            string hostName = Dns.GetHostName();
            string ipAddress = Dns.GetHostByName(hostName).AddressList[0].ToString();
            string userId = Convert.ToString(Session["UserId"]);
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputData = new Hashtable();
            inputData.Add("@strApplicationUserEmailID", userId);
            var strUserID = sqlDataAccess.ExecuteStoreProcedure("usp_Get_UserIdByEmail", inputData);
            int userID = Convert.ToInt32(strUserID);
            if (userRole == "Trader")
            {                      
                 sqlDataAccess = new SQLDataAccess();
                Hashtable hinput = new Hashtable();
                hinput.Add("@strUserReferenceID", strUserID);
                hinput.Add("@strRoleName", userRole);
                hinput.Add("@strUserIPAddresses", ipAddress);
                var queryRes = sqlDataAccess.ExecuteStoreProcedure("usp_sec_UpdateUserRole", hinput);
                
            }
            else if(userRole == "Signal Provider")
            {              
                sqlDataAccess = new SQLDataAccess();
                Hashtable hinput = new Hashtable();
                hinput.Add("@strUserReferenceID", strUserID);
                hinput.Add("@strRoleName", userRole);
                hinput.Add("@strUserIPAddresses", ipAddress);
                var queryRes = sqlDataAccess.ExecuteStoreProcedure("usp_sec_UpdateUserRole", hinput);
            }
            else if(userRole == "Referral")
            {               
                sqlDataAccess = new SQLDataAccess();
                Hashtable hinput = new Hashtable();
                hinput.Add("@strUserReferenceID", strUserID);
                hinput.Add("@strRoleName", userRole);
                hinput.Add("@strUserIPAddresses", ipAddress);
                var queryRes = sqlDataAccess.ExecuteStoreProcedure("usp_sec_UpdateUserRole", hinput);
            }
            else
            {

            }
            return View();
        }

        public JsonResult GetAllCountry()
        {
            List<mstCountry> countryList = new List<mstCountry>();
            //sqlDataAccess = new SQLDataAccess();
            //var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCountry");
            var result = SqlCommons.ExecuteStoreProcedure(globalVariables.SignalATMDbConn, StoredProcedures.GetCountry);
                
            foreach (DataRow dr in result.Rows)
            {
                countryList.Add(new mstCountry
                {
                    CountryID = Convert.ToInt32(dr["CountryID"]),
                    Country = dr["Country"].ToString()
                });
           }       

            return Json(countryList, JsonRequestBehavior.AllowGet);
        }    
        public JsonResult GetTimeZone()
        {
            List<Models.TimeZone> timeZoneList = new List<Models.TimeZone>();
            //sqlDataAccess = new SQLDataAccess();
            //var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTimeZones");
            var result = SqlCommons.ExecuteStoreProcedure(globalVariables.SignalATMDbConn,StoredProcedures.GetTimeZones);
            foreach (DataRow dr in result.Rows)
            {
                timeZoneList.Add(new Models.TimeZone
                {
                    TimeZoneID = Convert.ToInt32(dr["TimeZoneID"]),
                    TimeZoneDetails = dr["TimeZoneDetails"].ToString()
                });
            }
            return Json(timeZoneList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Enter_otp()
        {
            return View();
        }

        //public ActionResult Google()
        //{
        //    UserMgnt.loginType = "gmail";
        //    var Googleurl = "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=" + redirect_url + "&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile&client_id=" + googleplus_client_id;
        //    Response.Redirect(Googleurl); //831162440368 - gjpqkqbdj5mik8adij6s313qt51kqjor.apps.googleusercontent.com
        //    ViewBag.Message = "Your contact page.";
        //    return View();

        //}
        //public ActionResult FaceBookLogin()
        //{
        //    UserMgnt.loginType = "facebook";
        //    loginType = "facebook";
        //    var fb = new FacebookClient();
        //    var loginUrl = fb.GetLoginUrl(new
        //    {
        //        client_id = "581533035929980",
        //        redirect_uri = redirect_url,
        //        scope = "email" /// Add other permissions as needed

        //    });
        //    Response.Redirect(loginUrl.AbsoluteUri);
        //    return View();
        //}
        //public ActionResult LinkedinLogin()
        //{
        //    return View();
        //}

        //public ActionResult LinkedinLogin(string code, string state)
        //{
        //    LinkedInConnect.APIKey = "81oxu10ane59nm";
        //    LinkedInConnect.APISecret = "t8Js3GQOsGqavseM";
        //    LinkedInConnect.RedirectUrl = string.Format("{0}://{1}/{2}", Request.Url.Scheme, Request.Url.Authority, "https://localhost:44351/Home/Role_assigned");

        //    return View();
        //}
        [HttpPost]
        public EmptyResult Login1()
        {
            //if (!LinkedInConnect.IsAuthorized)
            //{
            //    LinkedInConnect.Authorize();
            //}

            return new EmptyResult();
        }

        //public ActionResult LinkedinLogin()
        //{
        //    return View();
        //}

        public ActionResult tr_documents()
        {
            return View();
        }
        public ActionResult tr_faq()
        {
            return View();
        }
        public ActionResult tr_global_profile()
        {
            return View();
        }
        public ActionResult tr_myportal_signals()
        {
            return View();
        }
       
        public ActionResult tr_my_signals()
        {
            var loginWith = string.Empty;
            if (UserMgnt.loginType != "")
            {
                loginWith = UserMgnt.loginType;
            }

            //if (loginWith == "facebook")
            //{
            //    string ac = Request.QueryString.ToString();
            //    string accessCode = Request.QueryString["code"].ToString();

            //    var fb = new FacebookClient();

            //    // throws OAuthException 
            //    dynamic result = fb.Post("oauth/access_token", new
            //    {
            //        // client_id = "1205696766486977",
            //        // client_secret = "c1e34106bf083041714176327ff502e0",//Sandeep 
            //           client_id = "581533035929980",
            //           client_secret = "237a909bb5c6b75fc6ec52b763f26515",
            //        redirect_uri = redirect_url,
            //        code = accessCode
            //    });

            //    var accessToken = result.access_token;
            //    var expires = result.expires;

            //    // Store the access token in the session
            //    Session["AccessToken"] = accessToken;

            //    // update the facebook client with the access token 
            //    fb.AccessToken = accessToken;

            //    // Calling Graph API for user info
            //    dynamic me = fb.Get("me?fields=id,name,email,gender,photos,location");
            //    //string id = me.id; // You can store it in the database                
            //    //string email = me.email;
            //    string name = me.name;
            //    string[] authorsList = name.Split(' ');

            //    string firstName = authorsList[0].ToString();
            //    string lastName = authorsList[1].ToString();

            //    otp otp = new otp();
            //    UserMgnt u = new UserMgnt();
            //    var genOtp = otp.GenerateOtp();
            //    SignalATMClass _SignalATM = new SignalATMClass();
            //    Hashtable ht_reg = new Hashtable();
            //    ht_reg.Add("@iUserEmail", me.email);
            //    ht_reg.Add("@iUserPassword", "");
            //    ht_reg.Add("@iUserOTP", genOtp);
            //    ht_reg.Add("@iFirstName", firstName);
            //    ht_reg.Add("@iLastName", lastName);
            //    ht_reg.Add("@iSocialMedia", true);
            //    ht_reg.Add("@iSocialMediaID", me.id);
            //    string strResult = _SignalATM.RegisterBySocialMedia(ht_reg);
            //    //if (strResult == "User has been registed.")
            //    //    return Json(new { success = true, result = "OK" }, JsonRequestBehavior.AllowGet);
            //    //else
            //    //    return Json(new { success = true, result = "Failed" }, JsonRequestBehavior.AllowGet);
            //}
            //if (loginWith =="gmail")
            //{
            //    String Parameters;
            //    ///string loginWith = Session["loginWith"].ToString();
            //    var url = Request.Url.Query;
            //    String queryString = url.ToString();
            //    char[] delimiterChars = { '=' };
            //    string[] words = queryString.Split(delimiterChars);
            //    string code = words[1];
            //    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
            //    webRequest.Method = "POST";
            //    Parameters = "code=" + code + "&client_id=" + googleplus_client_id + "&client_secret=" + googleplus_client_secret + "&redirect_uri=" + redirect_url + "&grant_type=authorization_code";
            //    byte[] byteArray = Encoding.UTF8.GetBytes(Parameters);
            //    webRequest.ContentType = "application/x-www-form-urlencoded";
            //    webRequest.ContentLength = byteArray.Length;
            //    Stream postStream = webRequest.GetRequestStream();
            //    // Add the post data to the web request
            //    postStream.Write(byteArray, 0, byteArray.Length);
            //    postStream.Close();

            //    WebResponse response = webRequest.GetResponse();
            //    postStream = response.GetResponseStream();

            //    StreamReader reader = new StreamReader(postStream);
            //    string responseFromServer = reader.ReadToEnd();

            //    GooglePlusAccessToken serStatus = JsonConvert.DeserializeObject<GooglePlusAccessToken>(responseFromServer);
            //    if (serStatus != null)
            //    {
            //        string accessToken = string.Empty;
            //        accessToken = serStatus.access_token;

            //        if (!string.IsNullOrEmpty(accessToken))
            //        {
            //            // This is where you want to add the code if login is successful.
            //            getgoogleplususerdataSerAsync(accessToken);
            //        }
            //    }
            //}
            return View();
        }
        public ActionResult tr_plan_payments()
        {
            return View();
        }
        public ActionResult tr_profile()
        {
            return View();
        }
        public ActionResult tr_signal_preferences()
        {

            return View();
        }
        public ActionResult tr_subscription_checkout()
        {

            return View();
        }
        public ActionResult tr_subscription_payment()
        {
            return View();
        }
        public ActionResult tr_subscription_plan()
        {
            return View();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult InvalidUser()
        {
            return View();
        }
        public ActionResult SendPasswordLink()
        {
            return View();
        }
        public ActionResult SavePasswordMail()
        {
            return View();
        }

        private int GetuserId(Hashtable hd)
        {
            var strUserID = SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.UserIDByEmail, hd);
            int userID = Convert.ToInt32(strUserID);
            return userID;
        }
        private int GetuserRoleId(Hashtable ht)
        {
            sqlDataAccess = new SQLDataAccess();
            var iRoleId = SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.GetRoleById, ht);
            int roleId = Convert.ToInt32(iRoleId);
            return roleId;
        }

        private string RegisterBySocialMedia(Hashtable hInputPara)
        {
            string result = SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.RegisterbySocialMedia, hInputPara);
            switch (result)
            {
                case "True":
                    return "User has been registed.";
                case "False":
                    return "User already exists.";
            }
            return result;
        }

        /// <summary>
        /// Saves OTP against a given email to DB for user validation in next steps
        /// </summary>
        /// <param name="email"></param>
        /// <param name="OTP"></param>
        /// <returns></returns>
        private bool SaveOTP(string email, string OTP)
        {
            // RegisterVerifyOTP
            bool executionStatus = false;

            try
            {
                Hashtable reg_User = new Hashtable();
                reg_User.Add("@strEmailAddress", email);
                reg_User.Add("@strOTP", email);
                var isSaveOTPSuccess = SqlCommons.ExecuteStoreProcedureStr(globalVariables.SignalATMDbConn, StoredProcedures.SaveOTP, reg_User);

                if (isSaveOTPSuccess == ExecutionStatus.SUCCESS.GetValue().ToString())
                {
                    Debug.logInfo(string.Format("OTP saved successful. Email:-{0}", email));
                    executionStatus = false;
                }
                else
                {
                    Debug.logInfo(string.Format("OTP not saved successful. Email:-{0}", email));
                }
            }
            catch (Exception ex)
            {
                Debug.logError(string.Format("Exception occured in Home.RegisterUser. Ex:- {0}", ex.Message));
            }
            return executionStatus;
        }
    }
}