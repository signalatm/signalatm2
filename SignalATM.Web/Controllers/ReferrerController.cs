﻿using InfoFreightSystem;
using Newtonsoft.Json;
//using Razorpay.Api;
using SignalATM.Commons;
using SignalATM.Web.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SignalATM.Web.Controllers
{
    public class ReferrerController : Controller
    {

        SQLDataAccess sqlDataAccess;
        Hashtable inputParams;
        public GlobalVariables globalVariables;
        public static string orderId;
        public ActionResult Index()
        {
            return View();
        }
        public ReferrerController()
        {
            try
            {
               // string emailId = "deepti.10m@gmail.com";
                string emailId=System.Web.HttpContext.Current.Session["UserId"].ToString();                            
                sqlDataAccess = new SQLDataAccess();
            //    globalVariables = new GlobalVariables(emailId);
                ViewData["WalletAmount"] = globalVariables.UserWalletBalance;
            }
            catch (Exception ex)
            {

            }
        }
        #region ReferrerProfile
        public ActionResult PersonalDetails()
        {
            ViewBag.UserEmail = Session["UserId"].ToString();
            ViewBag.userID = globalVariables.SecUserID;
            return View();
        }
        public string GetPersonalDetails()
        {
            string str = "";
            inputParams = new Hashtable();
            inputParams.Add("@UserID", globalVariables.SecUserID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPersonalDetails", inputParams);
            if (dt.Rows.Count == 0)
            {
                str = "0";
            }
            else
            {
                str = JsonConvert.SerializeObject(dt);
            }
            return str;
        }
        public ActionResult GetPersonalDetailsForGlobalProfile()
        {
            string str = "";
            try
            {

                inputParams = new Hashtable();
                inputParams.Add("@UserID", globalVariables.SecUserID);
                sqlDataAccess = new SQLDataAccess();
                String[] DocArray = new String[6];
                var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPersonalDetails", inputParams);

                if (dt.Rows.Count == 0)
                {
                    str = "0";
                }
                else
                {
                    DocArray[0] = dt.Rows[0]["UserName"].ToString();
                    DocArray[1] = dt.Rows[0]["AboutMe"].ToString();
                    DocArray[2] = dt.Rows[0]["Email"].ToString();
                    DocArray[3] = dt.Rows[0]["EntryDate"].ToString();
                    DocArray[4] = dt.Rows[0]["ApplicationUserPhoneNumber"].ToString();
                    DocArray[5] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserImage"]);
                }

                return Json(new { success = true, response = DocArray }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = true, response = str }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult SubmitPersonalDetails(string result)
        {
            try
            {
                var fileName = "";
                var path = "";
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    fileName = Path.GetFileName(file.FileName);
                    path = Path.Combine(Server.MapPath("~/Content/CSS/img"), fileName);
                    file.SaveAs(path);
                }
                byte[] theBytes = Encoding.UTF8.GetBytes(fileName);
                dynamic obj = JsonConvert.DeserializeObject(result);
                inputParams = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                inputParams.Add("@iUserID", globalVariables.SecUserID);
                inputParams.Add("@strUserName", obj.Name.ToString());
                inputParams.Add("@strDisplayName", obj.DisplayName.ToString());
                inputParams.Add("@strEmail", obj.Email.ToString());
                inputParams.Add("@strUserPhoneNO", obj.Phone.ToString());
                inputParams.Add("@iGender", obj.Gender.ToString());
                inputParams.Add("@strUserAddress", obj.Address.ToString());
                inputParams.Add("@iCountryID", obj.Country.ToString());
                inputParams.Add("@iTimeZone", obj.TimeZone.ToString());
                inputParams.Add("@strState", obj.State.ToString());
                inputParams.Add("@strCity", obj.City.ToString());
                inputParams.Add("@strAboutMe", obj.Aboutme.ToString());
                inputParams.Add("@iUserReferenceID", globalVariables.SecApplicationUserID);
                inputParams.Add("@strUserUrl", obj.Website.ToString());
                inputParams.Add("@bUserImage", theBytes);
                string resp = sqlDataAccess.ExecuteStoreProcedure("usp_SavePersonalDetails", inputParams);
                return Json(new { success = true, response = resp }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return Json(new { success = true, response = msg }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult EditPersonalDetails(string result)
        {
            try
            {
                var fileName = "";
                var path = "";
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    fileName = Path.GetFileName(file.FileName);
                    path = Path.Combine(Server.MapPath("~/Content/CSS/img"), fileName);
                    file.SaveAs(path);
                }
                byte[] theBytes = Encoding.UTF8.GetBytes(fileName);
                dynamic obj = JsonConvert.DeserializeObject(result);
                inputParams = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                inputParams.Add("@iUserID", globalVariables.SecUserID);
                inputParams.Add("@strUserName", obj.Name.ToString());
                inputParams.Add("@strDisplayName", obj.DisplayName.ToString());
                inputParams.Add("@strEmail", obj.Email.ToString());
                inputParams.Add("@strUserPhoneNO", obj.Phone.ToString());
                inputParams.Add("@iGender", obj.Gender.ToString());
                inputParams.Add("@strUserAddress", obj.Address.ToString());
                inputParams.Add("@iCountryID", obj.Country.ToString());
                inputParams.Add("@iTimeZone", obj.TimeZone.ToString());
                inputParams.Add("@strState", obj.State.ToString());
                inputParams.Add("@strCity", obj.City.ToString());
                inputParams.Add("@strAboutMe", obj.Aboutme.ToString());
                inputParams.Add("@iUserReferenceID", globalVariables.SecApplicationUserID);
                inputParams.Add("@strUserUrl", obj.Website.ToString());
                inputParams.Add("@bUserImage", theBytes);
                string resp = sqlDataAccess.ExecuteStoreProcedure("usp_UpdatePersonalDetails", inputParams);
                return Json(new { success = true, response = resp }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return Json(new { success = true, response = msg }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetContryCityStateGender()
        {
            inputParams = new Hashtable();
            inputParams.Add("@", globalVariables.SecUserID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCountry");
            var dt1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllStateByCountryID", inputParams);
            var dt2 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllCityByStateID", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public ActionResult GetCountries()
        {
            List<mstCountry> countryList = new List<mstCountry>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCountry");
            foreach (DataRow r in dt.Rows)
            {
                countryList.Add(new mstCountry
                {
                    CountryID = Convert.ToInt32(r["CountryID"]),
                    Country = r["Country"].ToString()
                });
            }
            return Json(new { success = true, CountryList = countryList }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetStates(int countryID)
        {
            List<mstState> stateList = new List<mstState>();
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            inputParams.Add("@iCountryID", countryID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllStateByCountryID", inputParams);
            foreach (DataRow r in dt.Rows)
            {
                stateList.Add(new mstState
                {
                    StateID = Convert.ToInt32(r["StateID"]),
                    StateName = r["StateName"].ToString()
                });
            }
            return Json(new { success = true, StateList = stateList }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCities(int stateID)
        {
            List<mstCity> cityList = new List<mstCity>();
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            inputParams.Add("@iStateID", stateID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllCityByStateID", inputParams);
            foreach (DataRow r in dt.Rows)
            {
                cityList.Add(new mstCity
                {
                    CityID = Convert.ToInt32(r["CityID"]),
                    CityName = r["CityName"].ToString()
                });
            }
            return Json(new { success = true, CityList = cityList }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetGenders()
        {
            List<mstGender> GenderList = new List<mstGender>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetGender");
            foreach (DataRow r in dt.Rows)
            {
                GenderList.Add(new mstGender
                {
                    GenderID = Convert.ToInt32(r["GenderID"]),
                    Gender = r["Gender"].ToString()
                });
            }
            return Json(new { success = true, Genders = GenderList }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetTimeZones()
        {
            List<Models.TimeZone> timezoneList = new List<Models.TimeZone>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTimeZones");
            foreach (DataRow r in dt.Rows)
            {
                timezoneList.Add(new Models.TimeZone
                {
                    TimeZoneID = Convert.ToInt32(r["TimeZoneID"]),
                    TimeZoneDetails = r["TimeZoneDetails"].ToString()
                });
            }
            return Json(new { success = true, TimeZoneslist = timezoneList }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ChangePassword(string email, string password)
        {
            UserMgnt u = new UserMgnt();
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            string pwd = u.Encrypt(password);
            inputParams.Add("@email", email);
            inputParams.Add("@password", pwd);
            string resp = sqlDataAccess.ExecuteStoreProcedure("usp_UpdatePassword", inputParams);
            return Json(new { success = true, response = resp }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GlobalProfile()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            DataTable periodDT = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPeriod");
            if (periodDT.Rows.Count > 0 && periodDT != null)
            {
                foreach (DataRow dr in periodDT.Rows)
                {
                    items.Add(new SelectListItem
                    {
                        Text = "Period " + dr["SignalPerformanceEarningsPeriodID"].ToString(),
                        Value = dr["SignalPerformanceEarningsPeriodID"].ToString()
                    });
                }
            }
            ViewBag.Period = items;
            return View();
        }

        public ActionResult Documents()
        {
            return View();
        }
        public ActionResult GetUserDocument()
        {
            try
            {
                inputParams = new Hashtable();
                inputParams.Add("@iUserID", globalVariables.SecUserID);
                sqlDataAccess = new SQLDataAccess();
                String[] DocArray = new String[6];
                var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserDocumentDetails", inputParams);
                DocArray[0] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserPhoto"]);
                DocArray[1] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserOnePageProfile"]);
                DocArray[2] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserIdentity"]);
                DocArray[3] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserAddressProof"]);
                DocArray[4] = dt.Rows[0]["UserBankDetails"].ToString();
                DocArray[5] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserBankChequeStatement"]);
                // string str = JsonConvert.SerializeObject(DocArray);
                return Json(new { success = true, response = DocArray }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = true, response = "Fail" }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult UploadUserDocument(string bankdetails)
        {
            try
            {
                int flagPhoto = 0;
                int flagUserProfile = 0;
                int flagIdProf = 0;
                int flagAddProf = 0;
                int flagBankStatement = 0;
                var defaultVal = new byte[16];
                byte[] userPhoto = defaultVal;
                byte[] profile = defaultVal;
                byte[] idProf = defaultVal;
                byte[] addressProf = defaultVal;
                byte[] bankStatement = defaultVal;

                String[] files = new String[5];
                var path = "";
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    files[i] = Path.GetFileName(file.FileName);
                    path = Path.Combine(Server.MapPath("~/Content/CSS/img"), files[i]);
                    file.SaveAs(path);
                    if (Request.Files.AllKeys[i] == "userPhoto")
                    {
                        flagPhoto = 1;
                        userPhoto = Encoding.UTF8.GetBytes(files[i]);
                    }
                    else if (Request.Files.AllKeys[i] == "profile")
                    {
                        profile = Encoding.UTF8.GetBytes(files[i]);
                        flagUserProfile = 1;
                    }
                    else if (Request.Files.AllKeys[i] == "idProf")
                    {
                        idProf = Encoding.UTF8.GetBytes(files[i]);
                        flagIdProf = 1;
                    }
                    else if (Request.Files.AllKeys[i] == "addProf")
                    {
                        addressProf = Encoding.UTF8.GetBytes(files[i]);
                        flagAddProf = 1;
                    }
                    else if (Request.Files.AllKeys[i] == "bankStatement")
                    {
                        bankStatement = Encoding.UTF8.GetBytes(files[i]);
                        flagBankStatement = 1;
                    }
                }

                inputParams = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                inputParams.Add("@iUserID", globalVariables.SecUserID);
                inputParams.Add("@iUserRole", globalVariables.RoleID);
                inputParams.Add("@imgUserPhoto", userPhoto);
                inputParams.Add("@imgUserProfile", profile);
                inputParams.Add("@imgUserIdentity", idProf);
                inputParams.Add("@imgUserAddressProof", addressProf);
                inputParams.Add("@strUserBankDetails", bankdetails);
                inputParams.Add("@imgUserChequeStatement", bankStatement);
                inputParams.Add("@bIsApproved", 1);
                inputParams.Add("@iFlagUserPhoto", flagPhoto);
                inputParams.Add("@iFlagUserProfile", flagUserProfile);
                inputParams.Add("@iFlagUserIdentity", flagIdProf);
                inputParams.Add("@iFlagUserAddressProof", flagAddProf);
                inputParams.Add("@iFlagUserChequeStatement", flagBankStatement);
                string resp = sqlDataAccess.ExecuteStoreProcedure("usp_UploadUserDocuments", inputParams);
                return Json(new { success = true, response = resp }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return Json(new { success = true, response = msg }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region Subscription Plan

        public ActionResult SubscriptionPlans()
        {
            ViewBag.RoleID = globalVariables.RoleID;
            //ViewBag.RoleID = globalVariables.RoleID;
            //DataSet userSubScriptionDT = new DataSet();
            //string userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
            //if (userId != "")
            //{
            //    ViewBag.RoleID = globalVariables.RoleID;
            //    sqlDataAccess = new SQLDataAccess();
            //    inputParams = new Hashtable();
            //    inputParams.Add("@iUserID", globalVariables.SecUserID);
            //    userSubScriptionDT = sqlDataAccess.GetDatasetExecuteStoredProcedure("usp_Get_SubcriptionPlan", inputParams);
            //}
            return View();
        }
        public ActionResult GetSubscriptionPlanDetails(int roleId)
        {
            // var planList = from a in UserPlan where (a.RoleID = roleId) && (a.StatusID = 1) select a;
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            List<UserPlan> planList = new List<UserPlan>();
            inputParams.Add("@iRoleId", roleId);
            var dataTable = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserPlan", inputParams);
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                planList.Add(new UserPlan
                {
                    UserPlansID = Convert.ToInt32(dataTable.Rows[i]["UserPlansID"]),
                    PlanName = dataTable.Rows[i]["PlanName"].ToString(),
                    RoleID = dataTable.Rows[i]["RoleID"].ToString(),
                    Fee = Convert.ToDecimal(dataTable.Rows[i]["Fee"]),
                    Commission = Convert.ToDouble(dataTable.Rows[i]["Commission"]),
                    FixedAmount = Convert.ToDouble(dataTable.Rows[i]["FixedAmount"]),
                    Validity = Convert.ToInt16(dataTable.Rows[i]["Validity"]),
                    EntryDate = Convert.ToDateTime(dataTable.Rows[i]["EntryDate"]),
                    PlanType = dataTable.Rows[i]["PlanType"].ToString(),
                    PlanPackage = dataTable.Rows[i]["PlanPackage"].ToString(),
                    PlanFeatures = dataTable.Rows[i]["PlanFeatures"].ToString()
                });
            }
            return Json(planList, JsonRequestBehavior.AllowGet);
        }
        public string SaveSubscriptionPlanDetails(string result)
        {
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVariables.SecUserID);
            inputParams.Add("@iPlanID", obj.UserPlansID.ToString());
            inputParams.Add("@siValidity", obj.Validity.ToString());
            inputParams.Add("@iStatusID", 1);
            inputParams.Add("@iPaymentStatusID", 1);
            inputParams.Add("@sdtFromDate", obj.FromDate.ToString());
            var response = sqlDataAccess.ExecuteStoreProcedure("usp_Save_UserPlanSubscriptions", inputParams);
            return response;
        }
        public ActionResult SubscriptionCheckOut(int planId)
        {
            ViewBag.UserId = globalVariables.SecUserID;
            ViewBag.UserPhoneNo = globalVariables.ApplicationUserPhoneNo;
            ViewBag.Email = globalVariables.ApplicationUserEmailID;
            // ViewBag.UserName = globalVariables.UserName;
            ViewBag.WalletBalance = globalVariables.UserWalletBalance;
            ViewBag.PlanID = planId;
            return View();
        }
        public string GetSubscribedUserDetails(int planId)
        {
            string result = "";
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVariables.SecUserID);
            inputParams.Add("@iPlanID", planId);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSubscribedPlanDetails", inputParams);
            if (dt.Rows.Count == 0)
            {
                result = "0";
            }
            else
            {
                result = JsonConvert.SerializeObject(dt);
            }
            return result;
        }
        public string GetPromocoupons()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPromoCoupons");
            string result = JsonConvert.SerializeObject(dt);
            return result;
        }
        //public ActionResult NextToPaymentAction(string result)
        //{
        //    Session["paymentInfo"] = result;
        //    string key = "rzp_test_6SmO037aAU6umV";
        //    string secret = "oSCggMVxeJvmbSWrjZ26MECq";
        //    System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
        //    RazorpayClient client = new RazorpayClient(key, secret);
        //    dynamic obj = JsonConvert.DeserializeObject(result);
        //    Dictionary<string, object> options = new Dictionary<string, object>();
        //    string amtStr = Convert.ToString(obj.NetPaymentSourceAmt);
        //    string[] arrayNetAmt = amtStr.Split('.');
        //    Int32 netAmt = Convert.ToInt32(arrayNetAmt[0] + arrayNetAmt[1]);
        //    options.Add("amount", netAmt); // amount in the smallest currency unit
        //    options.Add("receipt", "order_rcptid_11");
        //    options.Add("currency", "INR");
        //    options.Add("payment_capture", "0");
        //    Order order = client.Order.Create(options);
        //    orderId = order["id"].ToString();
        //    ViewData["OrderID"] = orderId;
        //    ViewData["netAmt"] = netAmt;
        //    return View();
        //}
        //[HttpPost]
        //public ActionResult SuccessPayment()
        //{
        //    string paymentId = Request.Form["razorpay_payment_id"];
        //    string razorpayOrderId = Request.Form["razorpay_order_id"];
        //    string razorpaySignature = Request.Form["razorpay_signature"];
        //    string key = "rzp_test_6SmO037aAU6umV";
        //    string secret = "oSCggMVxeJvmbSWrjZ26MECq";
        //    var paymentInfo = (string)Session["paymentInfo"];
        //    dynamic obj = JsonConvert.DeserializeObject(paymentInfo);

        //    Dictionary<string, object> input = new Dictionary<string, object>();
        //    input.Add("amount", obj.NetPaymentSourceAmt); // this amount should be same as transaction amount

        //    RazorpayClient client = new RazorpayClient(key, secret);

        //    Dictionary<string, string> attributes = new Dictionary<string, string>();

        //    attributes.Add("razorpay_payment_id", paymentId);
        //    attributes.Add("razorpay_order_id", Request.Form["razorpay_order_id"]);
        //    attributes.Add("razorpay_signature", Request.Form["razorpay_signature"]);

        //    Utils.verifyPaymentSignature(attributes);

        //    RazorpayClient client3 = new RazorpayClient(key, secret);
        //    Payment payment = client3.Payment.Fetch(paymentId);

        //    Dictionary<string, object> options = new Dictionary<string, object>();
        //    string amtStr = obj.NetPaymentSourceAmt.ToString();
        //    string[] amtArray = amtStr.Split('.');
        //    Int32 finalAmt = Convert.ToInt32(amtArray[0] + amtArray[1]);
        //    options.Add("amount", finalAmt);
        //    options.Add("currency", "INR");
        //    Payment paymentCaptured = payment.Capture(options);

        //    //dynamic obj1 = JsonConvert.DeserializeObject(attr);

        //    RazorpayClient client2 = new RazorpayClient(key, secret);
        //    Payment payment2 = client2.Payment.Fetch(paymentId);
        //    if (payment2 != null)
        //    {
        //        Int32 razorAmountRefunded = 0, razorWallet = 0, razorFee = 0, razorTax = 0;
        //        string razorPaymentId = payment2.Attributes["id"].ToString();
        //        Int32 razorPaymentAmt = payment2.Attributes["amount"];
        //        string razorPaymentCur = payment2.Attributes["currency"].ToString();
        //        string razorOrder_id = payment2.Attributes["order_id"].ToString();
        //        string razorMethod = payment2.Attributes["method"].ToString();
        //        razorAmountRefunded = payment2.Attributes["amount_refunded"];
        //        string razorRefundStatus = payment2.Attributes["refund_status"].ToString();
        //        string razorCardId = payment2.Attributes["card_id"].ToString();
        //        string razorBank = payment2.Attributes["bank"].ToString();
        //        if (payment2.Attributes["wallet"] != null)
        //        {
        //            razorWallet = Convert.ToInt32(payment2.Attributes["wallet"]);
        //        }
        //        string razorVpa = payment2.Attributes["vpa"].ToString();
        //        string razorEmail = payment2.Attributes["email"].ToString();
        //        string razorContact = payment2.Attributes["contact"].ToString();
        //        if (payment2.Attributes["fee"] != null)
        //        {
        //            razorFee = payment2.Attributes["fee"];
        //        }
        //        if (payment2.Attributes["tax"] != null)
        //        {
        //            razorTax = payment2.Attributes["tax"];
        //        }
        //        string razorErrorCode = payment2.Attributes["error_code"].ToString();
        //        string razorErrorDescription = payment2.Attributes["error_description"].ToString();
        //        string razorCreatedAt = payment2.Attributes["created_at"].ToString();

        //        sqlDataAccess = new SQLDataAccess();
        //        inputParams = new Hashtable();
        //        // dynamic obj = JsonConvert.DeserializeObject(paymentInfo);
        //        string tranzactionDetails = "Tranzaction Successfull";
        //        inputParams.Add("@iBillID", Convert.ToInt32(obj.BillID.ToString()));
        //        inputParams.Add("@iUserID", Convert.ToInt32(globalVariables.SecUserID));
        //        inputParams.Add("@WalletAmountUsed", Convert.ToDecimal(obj.WalletAmt.ToString()));
        //        inputParams.Add("@dTotalAmount", Convert.ToDecimal(obj.TotamAmt.ToString()));
        //        inputParams.Add("@strCouponCode", obj.CouponCode.ToString());
        //        inputParams.Add("@dLessDiscountAmount", Convert.ToDecimal(obj.LessDiscountAmt.ToString()));
        //        inputParams.Add("@iPaymentSourceID", 2);
        //        inputParams.Add("@NetPaymentSourceAmount", Convert.ToDecimal(obj.NetPaymentSourceAmt.ToString()));
        //        inputParams.Add("@iPaymentTypeID", 1);
        //        inputParams.Add("@strTransactionDetails", tranzactionDetails);
        //        inputParams.Add("@dGST", 0);
        //        inputParams.Add("@iPaymentStatusID", 1);
        //        inputParams.Add("@sRazorpaymentId", paymentId);
        //        inputParams.Add("@sRazorOrderID", razorpayOrderId);
        //        inputParams.Add("@sRazorSignatureID", razorpaySignature);
        //        inputParams.Add("@iAmount", razorPaymentAmt);
        //        inputParams.Add("@sCurrency", razorPaymentCur);
        //        inputParams.Add("@sMethod", razorMethod);
        //        inputParams.Add("@bRefundStatus", razorRefundStatus);
        //        inputParams.Add("@iAmountRefunded", razorAmountRefunded);
        //        inputParams.Add("@sCardId", razorCardId);
        //        inputParams.Add("@sBank", razorBank);
        //        inputParams.Add("@iWallet", razorWallet);
        //        inputParams.Add("@svpa", razorVpa);
        //        inputParams.Add("@sRazorEmail", razorEmail);
        //        inputParams.Add("@sRazorContact", razorContact);
        //        inputParams.Add("@iRazorFee", razorFee);
        //        inputParams.Add("@iRazorTax", razorTax);
        //        inputParams.Add("@sRazorErrorCode", razorErrorCode);
        //        inputParams.Add("@sRazorErrorDescription", razorErrorDescription);
        //        inputParams.Add("@sRazorCreatedAt", razorCreatedAt);
        //        var responce = sqlDataAccess.ExecuteStoreProcedure("usp_Save_UserPlanSubscriptionPayments", inputParams);
        //    }
        //    //  return PlanPayments();
        //    return RedirectToAction("PlanPayments", "Referrer");
        //}
        #endregion
        #region My Referrals
        public ActionResult MyProspects()
        {
          //  Session["UserId"] = "deepti.10m@gmail.com";//temp  
            return View();
        }
        public string GetMyProspectsEmails()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iReferrerID", globalVariables.SecUserID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetProspectsEmailList", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;

        }
        public string SaveEmailList(string EmailList)
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iReferrerID", globalVariables.SecUserID);
            inputParams.Add("@strEmailList", EmailList);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_AddReferrerEmails", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public ActionResult AddMyProspectsEmailList(string EmailList)
        {
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@strEmailList", EmailList);
            inputParam.Add("@iReferrerID", Convert.ToInt32(globalVariables.SecUserID));
            var ds = SqlCommons.ExecuteStoreProcedureDS(globalVariables.SignalATMDbConn, StoredProcedures.GetValidInvalidEmailList, inputParams);
            var myarray = new
            {
                Invalid = JsonConvert.SerializeObject(ds.Tables[0]),
                Valid = JsonConvert.SerializeObject(ds.Tables[1]),

            };
            return Json(myarray, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReferralList()
        {
            return View();
        }
        public string GetReferrerList()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iReferrerID", Convert.ToInt32(globalVariables.SecUserID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_MyReferralList", inputParam);
            string Result = JsonConvert.SerializeObject(dt);
            return Result;
        }
        public string RequestForRegistration(string EmailId, string Message, int seqNum)
        {
            string returnString = "";
            try
            {
                if (SendEmail(EmailId, Message))
                {
                    sqlDataAccess = new SQLDataAccess();
                    Hashtable inputParam = new Hashtable();
                    inputParam.Add("@iReferrerID", Convert.ToInt32(globalVariables.SecUserID));
                    inputParam.Add("@strTraderEmail", EmailId);
                    inputParam.Add("@iSequenceNo", Convert.ToInt32(seqNum));
                    var dt = sqlDataAccess.ExecuteStoreProcedure("usp_InsertReferrerMessages", inputParam);
                    returnString = dt;
                }
                else
                {
                    returnString = "Mail server is unavailable!";
                }
            }
            catch (Exception ex)
            {
                returnString = ex.Message;
            }
            return returnString;
        }
        public bool SendEmail(string email, string msg)
        {
            try
            {
                string email1 = "karapesandeep@gmail.com";
                MailMessage mail = new MailMessage();
                mail.To.Add(email1);
                // mail.From = new MailAddress(ConfigurationManager.AppSettings["FromAddress"].ToString());
                mail.From = new MailAddress("sandipkarape313@gmail.com");
                mail.Subject = "Request for registration...";
                string Body = "<br/><br/>";
                Body += msg + "<br/>";
                mail.Body = Body;
                mail.IsBodyHtml = true;
                mail.Priority = System.Net.Mail.MailPriority.High;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SmtpClient"].ToString();
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString());
                smtp.EnableSsl = true;//--- Donot change
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = new System.Net.NetworkCredential("sandipkarape313@gmail.com", "@sandeep1595#");
                smtp.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string GetMessagesForReferral(int Msgid)
        {
            string result = "";
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iMessage", Convert.ToInt32(Msgid));
            var Result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetReferralMessageBody", inputParam);
            result = JsonConvert.SerializeObject(Result);
            return result;
        }
        public ActionResult Performance()
        {
            return View();
        }
        public string GetPerformanceData()
        {
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            inputParams.Add("@iReferrerID", globalVariables.SecUserID);
            var response = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ReferrerPerformanceDetails", inputParams);
            string str = JsonConvert.SerializeObject(response);
            return str;

        }
        #endregion

        #region Transactions
        public ActionResult IntraTransfer()
        {
            return View();
        }
        public string TrasnsferAmountDeatils(decimal RequestAmout, decimal CurrentWallentAmount)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@decRequestAmout", Convert.ToDecimal(RequestAmout));
            inputParam.Add("@decReferrerCurrentWallentAmount", Convert.ToDecimal(CurrentWallentAmount));
            inputParam.Add("@iReferrerID", Convert.ToInt32(globalVariables.SecUserID));
            inputParam.Add("@iReferreRoleID", globalVariables.RoleID);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_SaveReferrerTransferRequest", inputParam);
            return "";

        }
        public string GetIntraTransferDetails()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iUserID", Convert.ToInt32(globalVariables.SecUserID));
            inputParam.Add("@iRoleID", globalVariables.RoleID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserIntraTransfer", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public ActionResult Earnings()
        {
            return View();
        }
        public ActionResult Cashouts()
        {
            return View();
        }

        public string AddCashoutAmountDeatils(decimal RequestAmout, decimal CurrentWallentAmount)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@decRequestAmout", Convert.ToDecimal(RequestAmout));
            inputParam.Add("@decCurrentWallentAmount", Convert.ToDecimal(CurrentWallentAmount));
            inputParam.Add("@iUserID", Convert.ToInt32(globalVariables.SecUserID));
            inputParam.Add("@iRoleID", globalVariables.RoleID);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_SaveUserCashoutEarningRequestDetails", inputParam);
            return "";
        }
        public string GetCashout()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVariables.SecUserID);
            inputParams.Add("@iRoleID", globalVariables.RoleID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserEarningCashoutRequest", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        public string GetWalletAmountByID()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVariables.SecUserID);
            inputParams.Add("@iRoleID", globalVariables.RoleID);

            var Result = sqlDataAccess.ExecuteStoreProcedure("usp_GetUserWalletBalance", inputParams);
            // string str = JsonConvert.SerializeObject(dt);
            return Result;
        }

        public ActionResult PlanPayments()
        {
            return View();
        }
        public string GetPlanPayment()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVariables.SecUserID);
            var Result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPlanPaymentList", inputParams);
            string str = JsonConvert.SerializeObject(Result);
            return str;
        }
        #endregion

        #region Success Story
        public ActionResult SuccessStory()
        {
            return View();
        }
        public string GetReferraReferrallList()
        {
            var Result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_ReferrerReferredSuccessStory");
            string str = JsonConvert.SerializeObject(Result);
            return str;
        }
        #endregion

        #region Support
        public ActionResult Tickets()
        {
            return View();
        }
        public string AddTicketStatus(string Subject, string Message)
        {
            var fileName = "";
            var path = "";
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                fileName = Path.GetFileName(file.FileName);
                path = Path.Combine(Server.MapPath("~/Content/CSS/img"), fileName);
                file.SaveAs(path);
            }
            byte[] theBytes = Encoding.UTF8.GetBytes(fileName);
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iFromID", Convert.ToInt32(globalVariables.SecUserID));
            inputParam.Add("@iToID", Convert.ToInt32(1));
            inputParam.Add("@iTicketSubjectID", Subject);
            inputParam.Add("@strMessage", Message);
            inputParam.Add("@iTicketStatusID", 1);
            inputParam.Add("@imgChooseScreen", theBytes);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_SaveTicketDetails", inputParam);
            return res;
        }
        public ActionResult GetTicketImageById(int TicketID)
        {
            try
            {
                inputParams = new Hashtable();
                inputParams.Add("@iID", TicketID);
                inputParams.Add("@strScreen", "Ticket");
                sqlDataAccess = new SQLDataAccess();
                var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserFile", inputParams);
                string DocArray = Encoding.ASCII.GetString((byte[])dt.Rows[0]["ChooseScreen"]);
                return Json(new { success = true, TicketImage = DocArray }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = true, TicketImage = "Fail" }, JsonRequestBehavior.AllowGet);
            }

        }
        public string AddTicketReplay(int TicketsID, string Message)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iTicketID", Convert.ToInt32(TicketsID));
            inputParam.Add("@strMessage", Convert.ToString(Message));
            inputParam.Add("@iTicketStatusID", Convert.ToInt32(1));
            inputParam.Add("@iResponsebyID", Convert.ToInt32(globalVariables.SecUserID));
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_SaveTicketResponses", inputParam);
            return res;
        }
        public string GetAllTicketStatusDetails()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iSecUserID", Convert.ToInt32(globalVariables.SecUserID));
            inputParam.Add("@sdtFromDate", string.Empty);
            inputParam.Add("@sdtToDate", string.Empty);
            inputParam.Add("@iTicketStatusID", 0);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTraderTicketsDetails", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetResponseByTicket(int TicketsID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iTicketID", Convert.ToInt32(TicketsID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTicketResponsesById", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetTicketResponseMsgByID(int TicketResponsesID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iTicketResponsesID", Convert.ToInt32(TicketResponsesID));
            string Result = sqlDataAccess.ExecuteStoreProcedure("usp_GetResponsesMsgById", inputParam);
            // string str = JsonConvert.SerializeObject(dt);
            return Result;
        }
        public JsonResult GetTicketStatusList()
        {

            List<mstTicketStatu> TickestStatusList = new List<mstTicketStatu>();
            sqlDataAccess = new SQLDataAccess();
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTicketStatusList");

            foreach (DataRow dr in result.Rows)
            {
                TickestStatusList.Add(new mstTicketStatu
                {
                    TicketStatusID = Convert.ToInt32(dr["TicketStatusID"]),
                    TicketStatus = dr["TicketStatus"].ToString()
                });
            }
            return Json(TickestStatusList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetTicketSubjectList()
        {
            List<mstTicketSubject> SubjectList = new List<mstTicketSubject>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTicketSubjectList");
            foreach (DataRow dr in dt.Rows)
            {
                SubjectList.Add(new mstTicketSubject
                {
                    TicketSubjectID = Convert.ToInt32(dr["TicketSubjectID"]),
                    Subject = dr["Subject"].ToString()
                });
            }
            return Json(new { success = true, TicketSubjectList = SubjectList }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult FAQs()
        {
            return View();
        }
        public ActionResult Feedback()
        {
            return View();
        }
        public ActionResult GetFeedbackSubjectList()
        {
            List<mstFeedbackSubject> FeedbackSubjectList = new List<mstFeedbackSubject>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFeedbackSubjectList");
            foreach (DataRow dr in dt.Rows)
            {
                FeedbackSubjectList.Add(new mstFeedbackSubject
                {
                    FeedbackSubjectID = Convert.ToInt32(dr["FeedbackSubjectID"]),
                    Subject = dr["Subject"].ToString()
                });
            }
            return Json(new { success = true, FeedbackSubjectList = FeedbackSubjectList }, JsonRequestBehavior.AllowGet);
        }

        public string AddFeedback(string comments, string subject)
        {
            var fileName = "";
            var path = "";
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                fileName = Path.GetFileName(file.FileName);
                path = Path.Combine(Server.MapPath("~/Content/CSS/img"), fileName);
                file.SaveAs(path);
            }
            byte[] theBytes = Encoding.UTF8.GetBytes(fileName);

            //  dynamic obj = JsonConvert.DeserializeObject(result);
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", Convert.ToInt32(globalVariables.SecUserID));
            inputParams.Add("@iFeedbackSubjectID", Convert.ToInt32(subject));
            inputParams.Add("@strFeedbackComment", Convert.ToString(comments));
            inputParams.Add("@imgFeedbackFile", theBytes);
            string resp = sqlDataAccess.ExecuteStoreProcedure("usp_SaveFeedback", inputParams);
            return "";
        }
        public ActionResult GetImageById(int FeedbackID)
        {
            try
            {
                inputParams = new Hashtable();
                inputParams.Add("@iID", FeedbackID);
                inputParams.Add("@strScreen", "Feedback");
                sqlDataAccess = new SQLDataAccess();
                var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserFile", inputParams);
                string DocArray = Encoding.ASCII.GetString((byte[])dt.Rows[0]["FeedbackFile"]);
                return Json(new { success = true, FeedBackImage = DocArray }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = true, FeedBackImage = "Fail" }, JsonRequestBehavior.AllowGet);
            }

        }
        public string GetAllFeedback()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iUserID", Convert.ToInt32(globalVariables.SecUserID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetmstFeedback", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        #endregion

        #region Take a Tour
        public ActionResult TakeATour()
        {
            return View();
        }
        #endregion
        public ActionResult SiteMap()
        {
            return View();
        }

    }
}