//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SignalATM.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TimeZone
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TimeZone()
        {
            this.secApplicationUsers = new HashSet<secApplicationUser>();
        }
    
        public int TimeZoneID { get; set; }
        public string TimeZoneDetails { get; set; }
        public Nullable<double> TimeZoneDiffrence { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<secApplicationUser> secApplicationUsers { get; set; }
    }
}
