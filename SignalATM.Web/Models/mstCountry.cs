//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SignalATM.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class mstCountry
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public mstCountry()
        {
            this.mstStates = new HashSet<mstState>();
        }
    
        public int CountryID { get; set; }
        public string Country { get; set; }
        public Nullable<int> CountryCode { get; set; }
        public int StatusID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mstState> mstStates { get; set; }
    }
}
