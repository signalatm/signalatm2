//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SignalATM.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class secUserLog
    {
        public int UserLogID { get; set; }
        public int UserID { get; set; }
        public string UserLogSessionID { get; set; }
        public string UserLoginID { get; set; }
        public System.DateTime UserLoginTime { get; set; }
        public System.DateTime UserLogLastAccessedOn { get; set; }
        public Nullable<int> UserLogoutBy { get; set; }
        public Nullable<System.DateTime> UserLogoutTime { get; set; }
        public string UserLogHostName { get; set; }
        public string UserLogIPAddress { get; set; }
        public string UserLogBrowserInfo { get; set; }
        public byte UserLogStatusID { get; set; }
    
        public virtual secUserLogStatu secUserLogStatu { get; set; }
        public virtual secUser secUser { get; set; }
    }
}
