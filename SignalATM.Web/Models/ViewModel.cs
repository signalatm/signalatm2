﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SignalATM.Web.Models
{
    public class ViewModel
    {
        public string SelectedRoleType { get; set; }
        public IEnumerable<SelectListItem> RoleTypes { get; set; }
    }
}