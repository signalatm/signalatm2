//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SignalATM.Web.Models
{
    using System;
    
    public partial class usp_insert_update_delete_select_mstValidityForConfig_Result
    {
        public int ValidityID { get; set; }
        public int Period { get; set; }
        public string PeriodUnit { get; set; }
        public string Status { get; set; }
        public System.DateTime EntryDate { get; set; }
    }
}
