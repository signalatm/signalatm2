﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignalATM.Web.Models
{
    public class mstFeedbackSubject
    {
        public int FeedbackSubjectID { get; set; }
        public string Subject { get; set; }

    }
}