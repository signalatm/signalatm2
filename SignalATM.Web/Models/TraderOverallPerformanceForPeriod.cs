//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SignalATM.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TraderOverallPerformanceForPeriod
    {
        public int TraderOverallPerformanceForPeriodID { get; set; }
        public int PeriodID { get; set; }
        public int MarketID { get; set; }
        public int TraderID { get; set; }
        public decimal SuccessRate { get; set; }
        public decimal TotalGainedPips { get; set; }
        public decimal TotalLostPips { get; set; }
        public decimal AverageGainedPips { get; set; }
        public decimal AverageLostPips { get; set; }
        public decimal GainedLostPipsRatio { get; set; }
        public int TotalSuccessSignal { get; set; }
        public int TotalFailedSignal { get; set; }
        public int TotalSignalCount { get; set; }
        public int TotalAbandonedSignalCount { get; set; }
        public System.DateTime StartFrom { get; set; }
        public System.DateTime ValidTill { get; set; }
        public int StatusID { get; set; }
        public System.DateTime EntryDate { get; set; }
    
        public virtual mstMarket mstMarket { get; set; }
    }
}
