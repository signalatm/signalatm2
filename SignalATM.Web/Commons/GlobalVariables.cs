﻿using CsQuery.ExtensionMethods.Internal;
using InfoFreightSystem;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace SignalATM.Commons
{
    public enum UserRoles
    {
        ADMIN = 1,
        TRADER = 2,
        REFERRER = 3,
        SIGNAL_PROVIDER = 4,      
        
        OTHER = 10
    }

    public enum RegistrationStatus
    {
        INVALID_EMAIL = 0,
        NEW_VAILD_EMAIL = 1,
        ALREADY_REGISTERED = 2
    }

    public enum ExecutionStatus
    {
        FAIL =0,
        SUCCESS = 1
    }

    public class GlobalVariables
    {
        public GlobalVariables()
        {
            SignalATMDbConn = new SqlConnection(Connection.SqlConnectionString);
            SignalATMDbConn.Open();
        }
        //To check whether userid 
        public GlobalVariables(int userId)
        {
            SignalATMDbConn = new SqlConnection(Connection.SqlConnectionString);
            SignalATMDbConn.Open();
            Hashtable hInput = new Hashtable();
            hInput.Add("@UserID", userId);
           
            DataTable dt = SqlCommons.ExecuteStoreProcedure(SignalATMDbConn, StoredProcedures.GetGlobalVariablesData, hInput);
            RoleID = Convert.ToInt32(dt.Rows[0]["RoleID"]);

            if (RoleID == UserRoles.OTHER.GetValue())
            {
                SecUserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                SecApplicationUserID = Convert.ToInt32(dt.Rows[0]["ApplicationUserID"]);
                RoleName = dt.Rows[0]["RoleName"].ToString();
                Debug.logInfo("GlobalVariables - Set for Other role");
            }

            if (RoleID == UserRoles.ADMIN.GetValue())
            {
                SecUserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                RoleID = Convert.ToInt32(dt.Rows[0]["RoleID"]);
                RoleName = dt.Rows[0]["RoleName"].ToString();
                UserType = dt.Rows[0]["UserTypeID"].ToString();
                SecApplicationUserID = Convert.ToInt32(dt.Rows[0]["ApplicationUserID"]);
                ApplicationUserEmailID = Convert.ToString(dt.Rows[0]["ApplicationUserEmailID"]);
                PeriodID = Convert.ToInt32(dt.Rows[0]["PeriodID"]);
                MarketID = Convert.ToInt32(dt.Rows[0]["MarketID"]);
                CountryID = Convert.ToInt32(dt.Rows[0]["CountryID"]);
                int marketID = Convert.ToInt32(dt.Rows[1]["MarketID"]);
                TimezoneID = Convert.ToInt32(dt.Rows[0]["TimeZoneID"]);
                Debug.logInfo("GlobalVariables - Set for ADMIN role");
            }

            if (RoleID == UserRoles.TRADER.GetValue())
            {
                SecApplicationUserID = Convert.ToInt32(dt.Rows[0]["ApplicationUserID"]);
                SecUserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                RoleID = Convert.ToInt32(dt.Rows[0]["RoleID"]);
                RoleName = dt.Rows[0]["RoleName"].ToString();
                UserType = dt.Rows[0]["UserTypeID"].ToString();
                ApplicationUserPhoneNo = dt.Rows[0]["ApplicationUserPhoneNumber"].ToString();
                ApplicationUserEmailID = dt.Rows[0]["ApplicationUserEmailID"].ToString();
                double walletAmt = Convert.ToDouble(dt.Rows[0]["WalletBalance"]);
                UserWalletBalance = (float)walletAmt;
                PlanID = Convert.ToInt32(dt.Rows[0]["PlanID"]);
                PeriodID = Convert.ToInt32(dt.Rows[0]["PeriodID"]);
                MarketID = Convert.ToInt32(dt.Rows[0]["MarketID"]);
                CountryID = Convert.ToInt32(dt.Rows[0]["CountryID"]);
                TimezoneID = Convert.ToInt32(dt.Rows[0]["TimeZoneID"]);
                PlanExpiryDate = Convert.ToDateTime(dt.Rows[0]["ValidTill"]);
                Debug.logInfo("GlobalVariables - Set for TRADER role");
            }

            if (RoleID == UserRoles.SIGNAL_PROVIDER.GetValue())
            {
                //SecApplicationUserID = Convert.ToInt32(dt.Rows[0]["ApplicationUserID"]);
                SecUserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                RoleID = Convert.ToInt32(dt.Rows[0]["RoleID"]);
                RoleName = dt.Rows[0]["RoleName"].ToString();
               // UserType = dt.Rows[0]["UserTypeID"].ToString();
                ApplicationUserPhoneNo = dt.Rows[0]["ApplicationUserPhoneNumber"].ToString();
                ApplicationUserEmailID = dt.Rows[0]["Email"].ToString();
                double walletAmt = Convert.ToDouble(dt.Rows[0]["WalletBalance"]);
                UserWalletBalance = (float)walletAmt;
               // PlanID = Convert.ToInt32(dt.Rows[0]["PlanID"]);
                PeriodID = Convert.ToInt32(dt.Rows[0]["PeriodID"]);
                MarketID = Convert.ToInt32(dt.Rows[0]["MarketID"]); 
                 CountryID = Convert.ToInt32(dt.Rows[0]["CountryID"]);
                //TimezoneID = Convert.ToInt32(dt.Rows[0]["TimeZoneID"]);
               // PlanExpiryDate = Convert.ToDateTime(dt.Rows[0]["ValidTill"]);
                PlanExpiryDate = Convert.ToDateTime(dt.Rows[0]["PlanValidTill"]);
                Debug.logInfo("GlobalVariables - Set for SIGNAL_PROVIDER role");
            }

            if (RoleID == UserRoles.REFERRER.GetValue())
            {
                SecApplicationUserID = Convert.ToInt32(dt.Rows[0]["ApplicationUserID"]);
                SecUserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                RoleID = Convert.ToInt32(dt.Rows[0]["RoleID"]);
                RoleName = dt.Rows[0]["RoleName"].ToString();
                UserType = dt.Rows[0]["UserTypeID"].ToString();
                ApplicationUserPhoneNo = dt.Rows[0]["ApplicationUserPhoneNumber"].ToString();
                ApplicationUserEmailID = dt.Rows[0]["ApplicationUserEmailID"].ToString();
                double walletAmt = Convert.ToDouble(dt.Rows[0]["WalletBalance"]);
                PlanID = Convert.ToInt32(dt.Rows[0]["PlanID"]);
                CountryID = Convert.ToInt32(dt.Rows[0]["CountryID"]);
                TimezoneID = Convert.ToInt32(dt.Rows[0]["TimeZoneID"]);
                PlanExpiryDate = Convert.ToDateTime(dt.Rows[0]["ValidTill"]);
                UserWalletBalance = (float)walletAmt;
                Debug.logInfo("GlobalVariables - Set for REFERER role");
            }
        }
        public int SecApplicationUserID { get; set; }
        public int SecUserID { get; set; }
        public string ApplicationUserEmailID { get; set; }
        public string UserName { get; set; }
        public string ApplicationUserPhoneNo { get; set; }
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public string UserType { get; set; }
        public int MarketID { get; set; }
        public int PeriodID { get; set; }
        public string UserImage { get; set; }
        public float UserWalletBalance { get; set; }
        public int PlanID { get; set; }
        public int CountryID { get; set; }
        public int TimezoneID { get; set; }
        public DateTime PlanExpiryDate { get; set; }
        public SqlConnection SignalATMDbConn { get; set; }
    }
}