﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignalATM.Commons
{
    public class StoredProcedures
    {
        public const string IsFirstLogin = "usp_IsFirstLogin";
        //public const string CheckEmailExists = "usp_CheckEmailExists";
        public const string CheckEmailExists = "usp_CheckEmailAddressExists";
        public const string UserLoginVerify = "usp_UserLoginVerify";
        public const string ActiveUser = "usp_ActiveUser";
        public const string GetGlobalVariablesData = "usp_GetGlobalVariablesData";
        public const string IsCountryAndTimezoneExist = "usp_IsCountryAndTimezoneExist";
        public const string RegisterUser = "usp_sec_RegisterUser";
        public const string UserIDByEmail = "usp_Get_UserIdByEmail";
        public const string SaveBasicConf = "usp_Save_BasicConfiguration";
        public const string UpdateUserRole = "usp_sec_UpdateUserRole";
        public const string GetCountries = "usp_GetCountry";
        public const string GetTimeZones = "usp_GetTimeZones";
        public const string InstrumentLivePrice = "usp_Set_InstrumentPriceLive";
        public const string GetInstrumentIdFromName = "usp_Get_InstrumentId";
        public const string UpdatePassword = "usp_UpdatePassword";
        public const string EmailAddressExists = "usp_CheckEmailAddressExists";
        public const string UpdateActivationEmailDate = "usp_UpdateActivationEmailDate";
        public const string RegisterbySocialMedia = "usp_sec_RegisterUserBySocialMedia";
        public const string GetRoleById = "usp_GetRoleById";

        public const string GetAllInstruments = "usp_GetAllInstruments";
        public const string GetCountry = "usp_GetCountry"; 
        public const string GetEmpanelmentDetails = "usp_GetEmpanelmentDetails";
        public const string GetSubcriptionPlan = "usp_Get_SubcriptionPlan";
        public const string GetValidInvalidEmailList = "usp_ValidInvalidEmailList";
        public const string GetTraderComparePerformance = "usp_Trader_ComparePerformance"; 
        public const string GetAllStateByCountryID = "usp_GetAllStateByCountryID";
        //public const string GetAllInstruments = "usp_GetAllInstruments";
        //public const string GetAllInstruments = "usp_GetAllInstruments";
        //public const string GetAllInstruments = "usp_GetAllInstruments";
        //public const string GetAllInstruments = "usp_GetAllInstruments";
        //public const string GetAllInstruments = "usp_GetAllInstruments";
        //public const string GetAllInstruments = "usp_GetAllInstruments";
        //public const string GetAllInstruments = "usp_GetAllInstruments";
        //public const string GetAllInstruments = "usp_GetAllInstruments";
        //public const string GetAllInstruments = "usp_GetAllInstruments";
        public const string RegisterUserRoleSelection = "usp_RegisterUserRoleSelection";
        public const string SaveOTP = "usp_SaveOTP";
        public const string RegisterVerifyOTP = "usp_RegisterUserVerifyOTP";
        public const string RegisterUserEmail = "usp_RegisterUserEmail";
        public const string RegisterUserPassword = "usp_RegisterUserEnterPassword";
    }
}