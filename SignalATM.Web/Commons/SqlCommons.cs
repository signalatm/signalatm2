﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace SignalATM.Commons
{
    public class SqlCommons
    {
        public static DataSet ExecuteStoreProcedureDS(SqlConnection conn, string spName, Hashtable htParams)
        {
            Debug.logDebug("entry - ExecuteStoreProcedureDS: " + spName);
            SqlCommand cmd = getCommandSP(conn, spName, htParams);
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        public static DataTable ExecuteStoreProcedure(SqlConnection conn, string spName, Hashtable htParams)
        {
            try
            {
                Debug.logDebug("entry - ExecuteStoreProcedure: " + spName);
                SqlCommand cmd = getCommandSP(conn, spName, htParams);
                return executeCommandDT(conn, cmd);
            }
            catch (Exception ex)
            {
                Debug.logError(ex);
                throw ex;
            }
        }

        public static string ExecuteStoreProcedureStr(SqlConnection conn, string spName, Hashtable htParams)
        {
            try
            {
                DataTable dt = ExecuteStoreProcedure(conn, spName, htParams);
                return Convert.ToString(dt.Rows[0][0]);
            }
            catch (Exception ex)
            {
                Debug.logError(ex);
                throw ex;
            }
        }
        public static DataTable ExecuteStoreProcedure(SqlConnection conn, SqlCommand cmd)
        {
            return executeCommandDT(conn, cmd);
        }

        public static DataTable ExecuteStoreProcedure(SqlConnection conn, string spName)
        {
            return executeCommandDT(conn, getCommandSP(conn, spName, null));
        }

        public static DataTable ExecuteStoreProcedure(SqlConnection con, string spName, string paramName, string paramValue)
        {
            try
            {
                Debug.logDebug("entry - ExecuteStoreProcedure: " + spName);
                SqlCommand cmd = getCommandSP(con, spName, null);
                cmd.Parameters.AddWithValue(paramName, paramValue);
                return executeCommandDT(con, cmd);
            }
            catch (Exception ex)
            {
                Debug.logError(ex);
                throw ex;
            }
        }

        public static DataTable ExecuteSQL(SqlConnection con, string sqlStmt)
        {
            return null;
        }

        private static DataTable executeCommandDT(SqlConnection conn, SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            dt.Load(dr);

            return dt;
        }

        private static SqlCommand getCommandSP(SqlConnection conn, string spName, Hashtable htParams)
        {
            SqlCommand cmd = new SqlCommand(spName, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            if (htParams != null && htParams.Count > 0)
            {
                cmd = addCommandParams(cmd, htParams);
            }
            return cmd;
        }

        private static SqlCommand addCommandParams(SqlCommand cmd, Hashtable ht)
        {
            Debug.logDebug("addCommandParams: " + cmd.CommandText);
            IDictionaryEnumerator IEnum = ht.GetEnumerator();
            while (IEnum.MoveNext())
            {
                Debug.logDebug("param: " + IEnum.Key.ToString() + ", value =" + IEnum.Value);
                cmd.Parameters.AddWithValue(IEnum.Key.ToString(), IEnum.Value);
            }
            return cmd;
        }
    }
}