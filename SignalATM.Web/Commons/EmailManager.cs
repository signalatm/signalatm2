﻿using OrderWorkAPI.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SignalATM.Commons
{
    public enum MessageType
    {
        RegistrationEmail,
        ActivationEmail,
        ForgotPassword,
        PasswordResetEmail
    }
    public class EmailManager
    {
        public static async System.Threading.Tasks.Task<Boolean> SendEmailAsync(string mailTo, MessageType Msgtype, string sParams)
        {
            EmailSimple emailSimple = new EmailSimple();
            StringBuilder sb_SendEmail = new StringBuilder();
            string uri = "";
            string reqCommand = "";
            string subject = "";
            string mailBody = "";

            switch (Msgtype)
            {
                case MessageType.RegistrationEmail:
                    {
                        subject = "User Registration. OTP: " + sParams;
                        uri = ConfigurationManager.AppSettings["DomainName"].ToString() + "/Home/ActivateUserAccount?email=" + mailTo;
                        reqCommand = "serviceproviders/sendactivationemail?mailTo=" + mailTo + "&mailSubject=" + subject + "&url=" + uri;
                        break;
                    }
                case MessageType.ActivationEmail:
                    {
                        subject = "User account activation mail";
                        uri = ConfigurationManager.AppSettings["DomainName"].ToString() + "/Home/ActivateUserAccount?email=" + mailTo;
                        reqCommand = "serviceproviders/sendactivationemail?mailTo=" + mailTo + "&mailSubject=" + subject + "&url=" + uri;
                        break;
                    }
                case MessageType.ForgotPassword:
                    {
                        subject = "Forgot Password";
                        reqCommand = "serviceproviders/SendPasswordResetEmail?mailTo=" + mailTo + "&mailSubject=" + subject + "&mailBody=" + mailBody;
                        break;
                    }
                case MessageType.PasswordResetEmail:
                    {
                        subject = "Forgot Password";
                        uri = ConfigurationManager.AppSettings["DomainName"].ToString() + "/Home/Forgot_PasswordDetails?email=" + mailTo;
                        reqCommand = "SendPasswordResetEmaillink? mailTo = " + mailTo + " & mailSubject = " + subject + " & url = " + uri;
                        break;
                    }
            }

            var values = new Dictionary<string, string>();
            values.Add("mailTo", mailTo);
            values.Add("mailSubject", subject);
            var content = new FormUrlEncodedContent(values);

            var client = new HttpClient();
            client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseUrl"].ToString());
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage resp = await client.GetAsync(reqCommand);
            return resp.IsSuccessStatusCode;
        }
    }
}