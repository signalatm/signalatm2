﻿using log4net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace SignalATM.Commons
{
    public class Debug
    {
        private static readonly ILogger logger = LoggerManager.GetLogger(Assembly.GetCallingAssembly(), "FileLogger");
        public static void logInfo(string message)
        {
            logger.Log(typeof(Debug), Level.Info, message, null);
        }

        public static void logError(string message)
        {
            logger.Log(typeof(Debug), Level.Error, message, null);
        }

        public static void logError(Exception ex)
        {
            logger.Log(typeof(Debug), Level.Error, ex.Message, ex);
            logDebug(ex.StackTrace);
            logDebug(ex.InnerException);
        }

        public static void logWarning(string message)
        {
            logger.Log(typeof(Debug), Level.Warn, message, null);
        }

        public static void logDebug(string message)
        {
            logger.Log(typeof(Debug), Level.Debug, message, null);
        }

        public static void logDebug(Exception message)
        {
            logger.Log(typeof(Debug), Level.Debug, message, null);
        }

        public static void logCritical(string message)
        {
            logger.Log(typeof(Debug), Level.Critical, message, null);
        }
    }
}