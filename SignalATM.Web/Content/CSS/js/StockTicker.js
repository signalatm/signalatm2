﻿if (!String.prototype.supplant) {
    String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = o[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    };
}

$(function () {

    var ticker = $.connection.stockTickerMini, // the generated client-side hub proxy
        up = '▲',
        down = '▼',
        $stockTable = $('#stockTable'),
        $stockTableBody = $stockTable.find('tbody'),
        rowTemplate = '<tr data-symbol="{InstrumentName}"><td>{InstrumentName}</td><td>{PriceBid}</td><td>{PriceAsk}</td><td>{LiveFeed}</td><td>{EntryDate}</td></tr>';

    function formatStock(stock) {
        return $.extend(stock, {
            PriceBid: stock.PriceBid.toFixed(2),
            PriceAsk: stock.PriceAsk.toFixed(2),
            LiveFeed: stock.LiveFeed.toFixed(2)
        });
    }

    function init() {
        ticker.server.getAllStocks().done(function (stocks) {
            $stockTableBody.empty();
            $.each(stocks, function () {
                var stock = formatStock(this);
                $stockTableBody.append(rowTemplate.supplant(stock));
            });
        });
    }

    // Add a client-side hub method that the server will call
    ticker.client.updateStockPrice = function (stock) {
        var displayStock = formatStock(stock),
            $row = $(rowTemplate.supplant(displayStock));

        $stockTableBody.find('tr[data-symbol=' + stock.InstrumentName + ']')
            .replaceWith($row);
    }

    // Start the connection
    $.connection.hub.logging = true;
    $.connection.hub.start().done(init);

});