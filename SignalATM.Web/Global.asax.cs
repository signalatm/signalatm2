﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SignalATM.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
           // SqlDependency.Start(ConfigurationManager.ConnectionStrings["SignalATMEntitiesNew"].ConnectionString);

        }
        protected void Application_End()
        {
           // SqlDependency.Stop(ConfigurationManager.ConnectionStrings["SignalATMEntitiesNew"].ConnectionString);
        }
    }
}
