﻿using log4net.Core;
using System;
using System.Reflection;

namespace SignalATM.Commons
{
    public class Debug
    {
        private static readonly ILogger logger = LoggerManager.GetLogger(Assembly.GetCallingAssembly(), "FileLogger");
        public static void LogInfo(string message)
        {
            logger.Log(typeof(Debug), Level.Info, message, null);
        }

        public static void LogError(string message)
        {
            logger.Log(typeof(Debug), Level.Error, message, null);
        }

        public static void LogError(Exception ex)
        {
            logger.Log(typeof(Debug), Level.Error, ex.Message, ex);
            LogDebug(ex.StackTrace);
            LogDebug(ex.InnerException);
        }

        public static void LogWarning(string message)
        {
            logger.Log(typeof(Debug), Level.Warn, message, null);
        }

        public static void LogDebug(string message)
        {
            logger.Log(typeof(Debug), Level.Debug, message, null);
        }

        public static void LogDebug(Exception message)
        {
            logger.Log(typeof(Debug), Level.Debug, message, null);
        }

        public static void LogCritical(string message)
        {
            logger.Log(typeof(Debug), Level.Critical, message, null);
        }
    }
}
